<?php

namespace AppBundle;

use AppBundle\Services\FastGoalService;
use Presta\SitemapBundle\Event\SitemapPopulateEvent;
use Presta\SitemapBundle\Sitemap\Url\UrlConcrete;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class AppBundle extends Bundle
{

    /**
     *
     */
    public function boot()
    {
        // get router
        $router = $this->container->get('router');
        // get event dispatcher
        $event = $this->container->get('event_dispatcher');

        // get entity manager
        $em = $this->container->get('doctrine')->getManager();


        //listen presta_sitemap.populate event
        $event->addListener(
            SitemapPopulateEvent::ON_SITEMAP_POPULATE,
            function (SitemapPopulateEvent $event) use ($router, $em) {

                //get absolute homepage url
                $url = $router->generate('homepage', array(), true);

                //add homepage url to the url set named homepage
                $event->getGenerator()->addUrl(new UrlConcrete($url, new \DateTime(), UrlConcrete::CHANGEFREQ_WEEKLY, 1), 'default');

                // get all posts
                $posts = $em->getRepository('AppBundle:Post')->findAll();

                // loop for posts
                foreach ($posts as $post) {

                    $url = $router->generate('post_detail', array('slug' => $post->getSlug()), true);
                    // add book
                    $event->getGenerator()->addUrl(
                        new UrlConcrete(
                            $url,
                            $post->getUpdated(),
                            UrlConcrete::CHANGEFREQ_MONTHLY,
                            0.9
                        ),
                        'details'
                    );
                }
            });
    }
}
