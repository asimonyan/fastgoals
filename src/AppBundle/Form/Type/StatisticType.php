<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PlayersType
 * @package AppBundle\Form\Type
 */
class StatisticType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('corner', null, array('label'=> 'Corner' ))
            ->add('fouls', null, array('label'=> 'Fouls' ))
            ->add('offsides', null, array('label'=> 'Offsides' ))
            ->add('totalShots', null, array('label'=> 'Total Shots' ))
            ->add('shotsOnTarget', null, array('label'=> 'Shots On Target' ))
            ->add('totalPasses', null, array('label'=> 'Total Passes' ))
            ->add('crosses', null, array('label'=> 'Crosses' ))
            ->add('throughBalls', null, array('label'=> 'Through Balls' ))
            ->add('longBalls', null, array('label'=> 'Long Balls' ))
            ->add('shortPasses', null, array('label'=> 'Short Passes' ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'allow_add' => true,
            'allow_delete' => true,
        ));
    }


    public function getName()
    {
        return 'statistic_type';
    }

}