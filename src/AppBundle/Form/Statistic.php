<?php

namespace AppBundle\Form;

use AppBundle\Form\Type\StatisticType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PlayersType
 * @package AppBundle\Form\Type
 */
class Statistic extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('home', new StatisticType())
            ->add('guest', new StatisticType())
        ;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'statistic';
    }

}