<?php
/**
 * Created by PhpStorm.
 * User: aram
 * Date: 6/25/15
 * Time: 6:31 PM
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation\Groups;


/**
* Class PlayerTeams
 * @package AppBundle\Entity
 *
 * @ORM\Table(name="player_team")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\PlayerTeamRepository")
 */
class PlayerTeam
{
    /**
     * old status value
     */
    const PLAYER_TEAM_OLD = 1;

    /**
     * current status value
     */
    const PLAYER_TEAM_CURRENT = 2;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected  $id;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="integer")
     *  @Groups({"main", "event_player"})
     */
    protected $number;

    /**
     * @ORM\ManyToOne(targetEntity="Player", inversedBy="playerTeams")
     * @ORM\JoinColumn(name="player_id", referencedColumnName="id")
     *
     * @Groups({"main", "event_player"})
     */
    protected $player;

    /**
     * @ORM\ManyToOne(targetEntity="Team", inversedBy="playerTeams", cascade={"persist"})
     * @ORM\JoinColumn(name="team_id", referencedColumnName="id")
     *
     * @Groups({"main", "event_player"})
     */
    protected $team;

    /**
     * @var date $start_date
     *
     * @ORM\Column(name="start_date", type="date")
     */
    protected $startDate;

    /**
     * @var date $end_date
     *
     * @ORM\Column(name="end_date", type="date")
     */
    protected $endDate;

    /**
     * @var integer
     *
     * @Groups({"main"})
     * @ORM\Column(name="status", type="smallint")
     */
    protected $status;

    /**
     *
     * @return string
     */
    public function __toString()
    {
        return '';
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param integer $number
     * @return PlayerTeam
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     * @return PlayerTeam
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime 
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     * @return PlayerTeam
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime 
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return PlayerTeam
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set player
     *
     * @param \AppBundle\Entity\Player $player
     * @return PlayerTeam
     */
    public function setPlayer(\AppBundle\Entity\Player $player = null)
    {
        $this->player = $player;

        return $this;
    }

    /**
     * Get player
     *
     * @return \AppBundle\Entity\Player 
     */
    public function getPlayer()
    {
        return $this->player;
    }

    /**
     * Set team
     *
     * @param \AppBundle\Entity\Team $team
     * @return PlayerTeam
     */
    public function setTeam(\AppBundle\Entity\Team $team = null)
    {
        $this->team = $team;

        return $this;
    }

    /**
     * Get team
     *
     * @return \AppBundle\Entity\Team 
     */
    public function getTeam()
    {
        return $this->team;
    }
}
