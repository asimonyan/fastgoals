<?php
/**
 * Created by PhpStorm.
 * User: aram
 * Date: 6/21/15
 * Time: 11:46 AM
 */

namespace AppBundle\Entity;

use AppBundle\Traits\AdPhoto;
use AppBundle\Traits\FileInfo;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Ad
 * @package AppBundle\Entity
 *
 * @ORM\Table(name="ad")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\AdRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Ad
{
    use FileInfo, AdPhoto;

    const AD_SENSE = 2;
    const RIGHT = 1;
    const AD = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    protected $title;

    /**
     * @var string slug
     *
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(type="string", length=50, unique=true, nullable=false)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="position", type="string", length=3)
     */
    protected $position;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    protected $url;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="smallint")
     */
    protected $type;

    /**
     * @var string
     *
     * @ORM\Column(name="width", type="string", length=3)
     */
    protected $width;

    /**
     * @var string
     *
     * @ORM\Column(name="height", type="string", length=3)
     */
    protected $height;

    /**
     * @return string
     */
    public function __toString()
    {
        return ($this->title) ? (string)$this->title : '';
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return $this
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set position
     *
     * @param string $position
     * @return $this
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set width
     *
     * @param string $width
     * @return $this
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Get width
     *
     * @return string
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set height
     *
     * @param string $height
     * @return $this
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get height
     *
     * @return string
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Ad
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }


    /**
     * @ORM\PreRemove()
     */
    public function preRemove()
    {
        // get logo file path
        $adPhoto = $this->getAbsolutePath() . $this->getAdPhotoName();

        // check file and remove
        if (file_exists($adPhoto)){
            unlink($adPhoto);
        }
    }

    /**
     * @ORM\PrePersist()
     */
    public function preUpload()
    {
        $this->uploadForAdPhoto();
    }

    /**
     * @return mixed
     */
    public function getImageUrl()
    {
        $imageUrl['src'] = null ;
        $imageUrl['width'] = $this->getWidth();
        $imageUrl['height'] =  $this->getHeight();

        // check photo name
        if($this->getAdPhotoName()){

            // get path
            $path = $this->getAbsolutePath() . $this->getAdPhotoName();

            if(is_file($path)){

                $imageUrl['src'] = $this->getAdPhotoDownloadLink() ;
            }
        }

        return $imageUrl;
    }

    public function getShowTitle()
    {
        return "";
    }

}
