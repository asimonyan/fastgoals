<?php
/**
 * Created by PhpStorm.
 * User: aram
 * Date: 6/24/15
 * Time: 12:08 AM
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation\Groups;


/**
 * Class MainPost
 * @package AppBundle\Entity
 *
 * @ORM\Table(name="main_post")
 * @ORM\Entity()
 *
 */
class MainPost extends Post
{
    /**
     * @var string
     *
     * @ORM\Column(name="score", type="string", length=10)
     * @Groups({"posts"})
     */
    protected $score;

    /**
     * @ORM\ManyToOne(targetEntity="Season", inversedBy="mainPost")
     * @ORM\JoinColumn(name="season_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $season;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Team", inversedBy="mainPostHomeTeam")
     * @ORM\JoinColumn(name="home_team_id", referencedColumnName="id", onDelete="SET NULL")
     * @Groups({"posts"})
     */
    protected  $teamHome;

    /**
     *
     * @ORM\OneToMany(targetEntity="Event", mappedBy="post", cascade={"all"})
     * @Groups({"posts"})
     */
    protected $event;

    /**
     * @var
     * @ORM\Column(name="statistic", type="array")
     */
    protected $statistic;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Team", inversedBy="mainPostGuestTeam")
     * @ORM\JoinColumn(name="guest_team_id", referencedColumnName="id", onDelete="SET NULL")
     * @Groups({"posts"})
     */
    protected  $teamGuest;


    /**
     * Set score
     *
     * @param string $score
     * @return MainPost
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score
     *
     * @return string 
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set season
     *
     * @param \AppBundle\Entity\Season $season
     * @return MainPost
     */
    public function setSeason(\AppBundle\Entity\Season $season = null)
    {
        $this->season = $season;

        return $this;
    }

    /**
     * Get season
     *
     * @return \AppBundle\Entity\Season 
     */
    public function getSeason()
    {
        return $this->season;
    }

    /**
     * Set teamHome
     *
     * @param \AppBundle\Entity\Team $teamHome
     * @return MainPost
     */
    public function setTeamHome(\AppBundle\Entity\Team $teamHome = null)
    {
        $this->teamHome = $teamHome;

        return $this;
    }

    /**
     * Get teamHome
     *
     * @return \AppBundle\Entity\Team 
     */
    public function getTeamHome()
    {
        return $this->teamHome;
    }

    /**
     * Set teamGuest
     *
     * @param \AppBundle\Entity\Team $teamGuest
     * @return MainPost
     */
    public function setTeamGuest(\AppBundle\Entity\Team $teamGuest = null)
    {
        $this->teamGuest = $teamGuest;

        return $this;
    }

    /**
     * Get teamGuest
     *
     * @return \AppBundle\Entity\Team 
     */
    public function getTeamGuest()
    {
        return $this->teamGuest;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->event = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add event
     *
     * @param \AppBundle\Entity\Event $event
     * @return MainPost
     */
    public function addEvent(\AppBundle\Entity\Event $event)
    {
        $this->event[] = $event;

        return $this;
    }

    /**
     * Remove event
     *
     * @param \AppBundle\Entity\Event $event
     */
    public function removeEvent(\AppBundle\Entity\Event $event)
    {
        $this->event->removeElement($event);
    }

    /**
     * Get event
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Set statistic
     *
     * @param array $statistic
     * @return MainPost
     */
    public function setStatistic($statistic)
    {
        $this->statistic = $statistic;

        return $this;
    }

    /**
     * Get statistic
     *
     * @return array 
     */
    public function getStatistic()
    {
        return $this->statistic;
    }
}
