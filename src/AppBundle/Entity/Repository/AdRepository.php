<?php
/**
 * Created by PhpStorm.
 * User: aram
 * Date: 7/12/15
 * Time: 1:20 PM
 */

namespace AppBundle\Entity\Repository;


use AppBundle\Entity\Ad;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * Class AdsRepository
 * @package AppBundle\Entity\Repository
 */
class AdRepository extends EntityRepository
{
    /**
     * @return array
     */
    public function findAllOrdered()
    {
        return $this->getEntityManager()
            ->createQuery('SELECT a FROM AppBundle:Ad a
                           WHERE a.type != :status
                           ORDER BY a.position ASC
                           ')
            ->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true)
            ->setParameter('status', Ad::RIGHT)
            ->getResult();

    }

    /**
     * @return array
     */
    public function findOneRight()
    {
        return $this->getEntityManager()
            ->createQuery('SELECT a FROM AppBundle:Ad a
                           WHERE a.type = :status
                           ORDER BY a.position ASC
                           ')
            ->setParameter('status', Ad::RIGHT)
            ->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true)
            ->setMaxResults(1)
            ->getOneOrNullResult();

    }
}
