<?php
/**
 * Created by PhpStorm.
 * User: aram
 * Date: 7/9/15
 * Time: 10:45 AM
 */

namespace AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * Class TeamRepository
 * @package AppBundle\Entity\Repository
 */
class TeamRepository extends EntityRepository
{

    const ENTITY = "AppBundle:Team";
    const COUNT = 28;

    /**
     * @param array $IDs
     * @return array
     */
    public function findByIDs($IDs = array())
    {
        if(empty($IDs)){
            return null ;
        }

        return $this->getEntityManager()
            ->createQuery("SELECT t
                           FROM AppBundle:Team t
                           INDEX BY t.id
                           WHERE t.id IN (:IDs)")
            ->setParameter('IDs', $IDs)
            ->getResult();
    }

    /**
     * This function is sued to get name and id, by slug
     *
     * @param $slug
     * @return array
     */
    public function findNameAndIdBySlug($slug)
    {
        $query = $this->getEntityManager()
            ->createQuery('SELECT t.id as id, t.name as name FROM ' . self::ENTITY . ' t
                               WHERE t.slug = :slug ')
            ->setParameter('slug', $slug)
        ;

        return $query->getOneOrNullResult();
    }

    /**
     * This function is used to get all teams by category id and teamdates
     *
     * @param string $categoryId int
     * @param string $startEndDate date
     * @param string $playerId
     * @return array
     */
    public function findAllTeams($categoryId = null , $playerId = null, $startEndDate = null )
    {
        $builder =
            $this->getEntityManager()->createQueryBuilder()
                ->select('t', 'e', 'c', 's','COUNT(DISTINCT e.id) AS goals')
                ->from(self::ENTITY, 't')
                ->leftJoin('t.playerTeams', 'pt')
                ->leftJoin('t.event', 'e')
                ->leftJoin('t.season', 's')
                ->leftJoin('s.category', 'c')
        ;

        // check category id
        if($categoryId){
            $builder
                ->andWhere('c.id = :categoryId')
                ->setParameter('categoryId', $categoryId);
        }

        // check startEndDate
        if($startEndDate){

            if($categoryId){
                $builder
                    ->andWhere('s.startDate <= :starDate and s.endDate >= :endDate')
                    ->setParameter('starDate', $startEndDate['startDate'])
                    ->setParameter('endDate', $startEndDate['endDate']);
            }

            if($playerId){
                $builder
                    ->andWhere('pt.startDate <= :startDate2 AND pt.endDate >= :endDate2')
                    ->setParameter('startDate2', $startEndDate['startDate'])
                    ->setParameter('endDate2', $startEndDate['endDate']);
            }
        }

        // check playerId
        if($playerId){
            $builder
                ->leftJoin('pt.player', 'player')
                ->andWhere('player.id = :plId')
                ->setParameter('plId', $playerId);
        }

        $builder
            ->orderBy('goals', 'DESC')
            ->addOrderBy('t.name', 'ASC')
            ->groupBy('t.id')
            ->setMaxResults(28)
        ;

        $builder->getQuery()->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true);

        return $builder->getQuery()->getResult();
    }

    /**
     * @param null $term
     * @param null $leagueId
     * @param null $playerId
     * @param null $date
     * @return array
     */
    public function findAllTeamsBySearch($term = null, $leagueId = null, $playerId = null, $date = null)
    {
        $builder = $this->getEntityManager()->createQueryBuilder();

        $builder
            ->select('t.name as name', 't.slug as slug')
            ->from(self::ENTITY, 't')
            ->leftJoin('t.season', 's')
            ->leftJoin('s.category', 'c');

        // check term
        if($term){
            $builder
                ->andWhere('t.name Like :name')
                ->setParameter('name', '%' . $term . '%')
            ;
        }

        // check league
        if($leagueId){
            $builder
                ->andWhere('c.id = :catId')
                ->setParameter('catId', $leagueId);
        }

        // check player
        if($playerId){
            $builder
                ->leftJoin('t.playerTeams', 'pt')
                ->leftJoin('pt.player', 'player')
                ->andWhere('player.id = :plId')
                ->setParameter('plId', $playerId);
        }

        // check date
        if($date){
            $builder
                ->andWhere('s.startDate <= :startDate AND s.endDate >= :endDate')
                ->setParameter('startDate', $date['startDate'])
                ->setParameter('endDate', $date['endDate']);
        }

        $builder
            ->orderBy('t.name')
            ->groupBy('t.id')

        ;

        $builder->getQuery()->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true);

        return $builder->getQuery()->getResult();
    }

}
