<?php
/**
 * Created by PhpStorm.
 * User: aram
 * Date: 7/7/15
 * Time: 8:04 PM
 */

namespace AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\PlayerTeam;

/**
 * Class PlayerTeamRepository
 * @package AppBundle\Entity\Repository
 */
class PlayerTeamRepository extends EntityRepository
{

    /**
     * @param $team
     * @param int $current
     * @return array
     */
    public function findAllByTeam($team, $current = 0)
    {
        if((int)$current > 0) {

            $status = PlayerTeam::PLAYER_TEAM_OLD;
        }
        else {

            $status = PlayerTeam::PLAYER_TEAM_CURRENT;
        }

        $result = $this->getEntityManager()
            ->createQuery("SELECT pt, p, t
                                       FROM AppBundle:PlayerTeam pt
                                       INDEX BY pt.id
                                       LEFT JOIN pt.team t
                                       LEFT JOIN pt.player p
                                       WHERE t.id = :team
                                       AND pt.status >= :status
                                       ORDER BY p.firstName")
            ->setParameter('team', $team)
            ->setParameter('status', $status)
            ->getResult();

        return $result;
    }
}
