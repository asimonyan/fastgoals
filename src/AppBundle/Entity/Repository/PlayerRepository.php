<?php
/**
 * Created by PhpStorm.
 * User: aram
 * Date: 7/9/15
 * Time: 10:45 AM
 */

namespace AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;


/**
 * Class PlayerRepository
 * @package AppBundle\Entity\Repository
 */
class PlayerRepository extends EntityRepository
{

    const ENTITY = "AppBundle:Player";

    /**
     * @param array $IDs
     * @return array
     */
    public function findByIDs($IDs = array())
    {
        if(empty($IDs)){
            return null ;
        }

        return $this->getEntityManager()
            ->createQuery("SELECT p
                           FROM AppBundle:Player p
                           INDEX BY p.id
                           WHERE p.id IN (:IDs)")
            ->setParameter('IDs', $IDs)
            ->getResult();
    }

    /**
     * @param $slug
     * @return array
     */
    public function findFullNameAndIdBySlug($slug)
    {
        $query = $this->getEntityManager()
            ->createQuery('SELECT p.id as id, p.firstName as fName, p.lastName as lName FROM ' . self::ENTITY . ' p
                               WHERE p.slug = :slug ')
            ->setParameter('slug', $slug)
        ;

        return $query->getOneOrNullResult();
    }


    /**
     * @param string $leagueId
     * @param string $teamId
     * @param string $date
     * @return array
     */
    public function findAllPlayers($leagueId = null, $teamId = null, $date = null)
    {
        // create query builder
        $builder = $this->getEntityManager()->createQueryBuilder();

        $builder
            ->select('p', 't', 'c', 'pt', 's', 'c')
            ->addSelect('(SELECT COUNT(DISTINCT e.id) FROM AppBundle:Event e WHERE e.team = t and e.player = p AND e.type = 1) AS goals')
            ->from(self::ENTITY, 'p')
            ->leftJoin('p.playerTeams', 'pt')
            ->leftJoin('pt.team', 't')
            ->leftJoin('t.season', 's')
            ->leftJoin('s.category', 'c')
        ;

        // check league
        if($leagueId){

            $builder
                ->andWhere('c.id = :leagueId')
                ->setParameter('leagueId', $leagueId);
        }

        // check team
        if($teamId){

            $builder
                ->andWhere('t.id = :teamId')
                ->setParameter('teamId', $teamId)
            ;
        }

        // check date
        if($date){

            if($leagueId){
                $builder
                    ->andWhere('s.startDate <= :startDate AND s.endDate >= :endDate')
                    ->setParameter('startDate', $date['startDate'])
                    ->setParameter('endDate', $date['endDate']);
            }

            $builder
                ->andWhere('pt.startDate <= :startDate2 AND pt.endDate >= :endDate2')
                ->setParameter('startDate2', $date['startDate'])
                ->setParameter('endDate2', $date['endDate']);
        }

        $builder
            ->orderBy('goals', 'DESC')
            ->groupBy('p.id')
            ->setMaxResults(28)
        ;


        $builder->getQuery()->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true);

        return $builder->getQuery()->getResult();
    }

    /**
     * @param null $term
     * @param null $leagueId
     * @param null $teamId
     * @param null $date
     * @return array
     */
    public function findAllPlayerBySearch($term = null, $leagueId = null, $teamId = null, $date = null)
    {
        $builder = $this->getEntityManager()->createQueryBuilder();

        $builder
            ->select('p.firstName as fName', 'p.lastName as lName', 'p.slug as slug')
            ->from(self::ENTITY, 'p')
            ->leftJoin('p.playerTeams', 'pt')
            ->leftJoin('pt.team', 't')
            ->leftJoin('t.season', 's')
            ->leftJoin('s.category', 'c');
        ;

        // check term
        if($term){

            $term = trim($term);
            $termResult = (preg_split('/\s+/', $term));

            if(count($termResult) == 2){
                $builder
                    ->where('(p.firstName LIKE :name1 AND p.lastName LIKE :name2) OR (p.firstName LIKE :name2 AND p.lastName LIKE :name1) ')
                    ->setParameter('name1', '%' . $termResult[0] . '%')
                    ->setParameter('name2', '%' . $termResult[1] . '%');
            }
            else{
                $builder
                    ->where('(p.firstName LIKE :name OR p.lastName LIKE :name)')
                    ->setParameter('name', '%' . $term . '%');
            }
        }

        // check league
        if($leagueId){
            $builder
                ->andWhere('c.id = :catId')
                ->setParameter('catId', $leagueId);
        }

        // check player
        if($teamId){
            $builder
                ->andWhere('t.id = :teamId')
                ->setParameter('teamId', $teamId);
        }

        // check date
        if($date){
            $builder
                ->andWhere('s.startDate <= :startDate AND s.endDate >= :endDate')
                ->andWhere('pt.startDate <= :startDate AND pt.endDate >= :endDate')
                ->setParameter('startDate', $date['startDate'])
                ->setParameter('endDate', $date['endDate']);
        }

        $builder
            ->orderBy('p.firstName')
            ->groupBy('p.id')
        ;

        $builder->getQuery()->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true);

        return $builder->getQuery()->getResult();
    }
}
