<?php
/**
 * Created by PhpStorm.
 * User: aram
 * Date: 7/12/15
 * Time: 2:15 PM
 */

namespace AppBundle\Entity\Repository;

use AppBundle\Controller\MainController;
use AppBundle\Entity\Event;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\PersistentCollection;
use Doctrine\ORM\Query;

/**
 * Class PostRepository
 * @package AppBundle\Entity\Repository
 */
class PostRepository extends EntityRepository
{
    /**
     * @param $offset
     * @param $limit
     * @param $l
     * @param $t
     * @param $n
     * @param $m
     * @param $p
     * @param $d
     * @return array
     */
    public function findByPosts($offset, $limit, $l, $t, $n, $p, $d, $m)
    {
        $builder = $this->getEntityManager()->createQueryBuilder();

        $builder
            ->select('p')
            ->from('AppBundle:Post', 'p')
            ->leftJoin('AppBundle:MainPost', 'mp', 'WITH', 'mp.id = p.id')
        ;

        if(!is_null($m) && $m == MainController::TOP_POST){
            $builder
                ->innerJoin('AppBundle:TopPost', 'tp', 'WITH', 'tp.id = p.id')
                ->leftJoin('tp.team', 'tpt');



            // check team id
            if($t){
                $builder
                    ->andWhere('tpt.id = :teamId ')
                    ->setParameter('teamId', $t)
                ;

            }

            // check player
            if($p){
                $builder
                    ->leftJoin('tp.player', 'tpp')
                    ->andWhere('tpp.id = :playerId')
                    ->setParameter('playerId', $p)
                ;
            }

            if($d){
                $builder
                    ->andWhere('p.postDate >= :startDate AND p.postDate <= :endDate')
                    ->setParameter('startDate', $d['startDate'])
                    ->setParameter('endDate', $d['endDate'])
                ;

            }


        }
        else{

            $builder
                ->leftJoin('AppBundle:TopPost', 'tp', 'WITH', 'tp.id = p.id');

            // check category id
            if($l){

                $builder
                    ->leftJoin('mp.season', 's')
                    ->leftJoin('s.category', 'c')
                    ->leftJoin('tp.season', 'tps')
                    ->leftJoin('tps.category', 'tpsc')
                    ->andWhere('c.id = :catId or tpsc.id = :catId')
                    ->setParameter('catId', $l)
                ;

            }
            $builder
                ->leftJoin('tp.team', 'tpt');


            // check team id
            if($t){
                $builder
                    ->leftJoin('mp.teamHome', 'th')
                    ->leftJoin('mp.teamGuest', 'tg')
                    ->andWhere('th.id = :teamId OR tg.id = :teamId OR tpt.id = :teamId ')
                    ->setParameter('teamId', $t)
                ;

            }

            // check player
            if($p){
                $builder
                    ->leftJoin('mp.event', 'mpe')
                    ->leftJoin('mpe.player', 'mpep')
                    ->leftJoin('tp.player', 'tpp')
                    ->andWhere('tpp.id = :playerId OR (mpe.type = :event_type AND mpep.id = :playerId )')
                    ->setParameter('playerId', $p)
                    ->setParameter('event_type', Event::EVENT_TYPE_GOAL)
                ;
            }

            if($d){
                $builder
                    ->andWhere('p.postDate >= :startDate AND p.postDate <= :endDate')
                    ->setParameter('startDate', $d['startDate'])
                    ->setParameter('endDate', $d['endDate'])
                ;

            }

            if(!is_null($m) && $m == MainController::MOST_POPULAR){
                $builder
                    ->orderBy('p.views', "DESC");
            }
        }

        $builder
            ->addOrderBy('p.postDate', "DESC")
            ->addOrderBy('p.position', "DESC")
            ->addOrderBy('p.id', "DESC")
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->groupBy('p.id')
        ;

        $builder->getQuery()->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true);

        return $builder->getQuery()->getResult();
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function findPostSlug($slug)
    {
        $builder = $this->getEntityManager()->createQueryBuilder();

        $builder
            ->select('p')
            ->from('AppBundle:Post', 'p')
            ->where('p.slug = :slug')
            ->setParameter('slug', $slug)
        ;

        $builder->getQuery()->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true);
        return $builder->getQuery()->getOneOrNullResult();
    }

    /**
     * @param $post
     * @return array
     */
    public function findByRightPosts($post)
    {
        // get doctrine filters
        $filters = $this->getEntityManager()->getFilters();

        // enable filter
        $filters->enable('publish_post');

        $query = $this->getEntityManager()
            ->createQuery('SELECT p FROM AppBundle:Post p
                               WHERE p.id != :post
                               ORDER BY p.postDate DESC')
            ->setParameter('post', $post)
//            ->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true)
            ->setMaxResults(9);


        return $query->getResult();
    }


    /**
     * @param $post
     * @return array
     */
    public function findByRightPostsForTop($post)
    {
        $teams = $post->getTeam();
        $teamsIds = $this->getIds($teams);

        $players = $post->getPlayer();
        $playersIds = $this->getIds($players);

        $seasons = $post->getSeason();
        $seasonsIds = $this->getIds($seasons);

        // get doctrine filters
        $filters = $this->getEntityManager()->getFilters();

        // enable filter
        $filters->enable('publish_post');

        $query = $this->getEntityManager();

        $builder = $query->createQueryBuilder();

        $builder
            ->addSelect("p")
            ->from('AppBundle:Post', 'p')
            ->leftJoin('AppBundle:TopPost', 'tp', 'WITH', 'tp.id = p.id')
            ->leftJoin('tp.team', 'topTeam')
            ->leftJoin('tp.player', 'topPlayer')
            ->leftJoin('tp.season', 'topSeason')

            ->leftJoin('AppBundle:MainPost', 'mp', 'WITH', 'mp.id = p.id')
            ->leftJoin('mp.teamHome', 'mainTeamHome')
            ->leftJoin('mainTeamHome.playerTeams', 'homePlayerTeam')
            ->leftJoin('homePlayerTeam.player', 'player1')

            ->leftJoin('mp.teamGuest', 'mainTeamGuest')
            ->leftJoin('mainTeamGuest.playerTeams', 'guestPlayerTeam')
            ->leftJoin('guestPlayerTeam.player', 'player2')

            ->leftJoin('mp.season', 'mainSeason')

            ->where('p.id != :post')
            ->setParameter('post', $post->getId())

            ->andWhere('topTeam in (:teamsIds) or topPlayer in (:playersIds) or topSeason in (:seasonsIds)
             or mainTeamHome in (:teamsIds) or mainTeamGuest in (:teamsIds) or mainSeason in (:seasonsIds)
             or player1 in (:playersIds) or player2 in (:playersIds)
             ')

            ->setParameter('teamsIds', $teamsIds)
            ->setParameter('playersIds', $playersIds)
            ->setParameter('seasonsIds', $seasonsIds)


            ->groupBy('p.id')
            ->setMaxResults(9)
        ;

        return $builder->getQuery()->getResult();
    }

    /**
     * @return mixed
     */
    public function getPostsMaxViews() {
        $query = $this->getEntityManager()
            ->createQuery('SELECT MAX(p.views) AS maxViews FROM AppBundle:Post p');

        return $query->getSingleResult();
    }


    /**
     * @param $slug
     * @return mixed
     */
    public function updateOneBySlug($slug)
    {
        $query = $this->getEntityManager()
            ->createQuery('Update AppBundle:Post p set p.views = p.views + 1
                           WHERE p.slug = :slug')
                           ->setParameter('slug', $slug);
        return $query->execute();
    }

    /**
     * @param PersistentCollection $collections
     * @return array
     */
    private function getIds(PersistentCollection $collections)
    {
        $ids = array();

        if($collections){
            foreach( $collections as $collection){
                $ids = $collection->getId();
            }
        }

        return $ids;
    }
}