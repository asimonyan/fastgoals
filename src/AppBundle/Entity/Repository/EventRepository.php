<?php
/**
 * Created by PhpStorm.
 * User: aram
 * Date: 7/13/15
 * Time: 3:14 PM
 */

namespace AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;


class EventRepository extends EntityRepository
{
    /**
     * @param $slug
     * @return mixed
     */
    public function findOneByPlayers($slug)
    {

        $builder = $this->getEntityManager()->createQueryBuilder();

        $builder
            ->select('e', 'p', 'pl', 't', 'pt')
            ->from('AppBundle:Event', 'e')
            ->leftJoin('e.post', 'p')
            ->leftJoin('e.player', 'pl')
            ->leftJoin('pl.playerTeams', 'pt')
            ->leftJoin('e.team', 't')
            ->where('p.slug = :slug')
            ->setParameter('slug', $slug)

        ;

        $builder->getQuery()->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true);

        return $builder->getQuery()->getResult();
    }

    /**
     * @param $team
     * @param $post
     * @return array
     */
    public function findAllByTeamAndPost($team, $post){
        $posts = $this->getEntityManager()
            ->createQuery("SELECT e, p, pl
                                       FROM AppBundle:Event e
                                       LEFT JOIN e.post p
                                       LEFT JOIN e.player pl
                                       WHERE p.id = :post")
            ->setParameter('post', $post)
            ->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true)
            ->getResult();

        $postsByID = array();
        foreach($posts as $value)
        {
            $postsByID[$value->getPlayer()->getId()][] = $value;
        }

        $players = $this->getEntityManager()
            ->createQuery("SELECT p, t, pt
                                       FROM AppBundle:PlayerTeam pt
                                       LEFT JOIN pt.team t
                                       LEFT JOIN pt.player p
                                       WHERE t.id = :team")
            ->setParameter('team', $team)
            ->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true)
            ->getResult();

        $teamPlayers = array();
        $i = 0;

        foreach($players as $value)
        {
            $teamPlayers[$i]['info'] = $value;
            if(isset($postsByID[$value->getPlayer()->getId()]))
            {
                $teamPlayers[$i]['events'] = $postsByID[$value->getPlayer()->getId()];
            }
            $i++;
        }

        return array_values($teamPlayers);

    }
}