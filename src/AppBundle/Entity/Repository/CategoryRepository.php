<?php
/**
 * Created by PhpStorm.
 * User: aram
 * Date: 7/7/15
 * Time: 8:04 PM
 */

namespace AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * Class CategoryRepository
 * @package AppBundle\Entity\Repository
 */
class CategoryRepository extends EntityRepository
{
    const ENTITY = "AppBundle:Category";

    /**
     * @param $slug
     * @return array
     */
    public function findNameAndIdBySlug($slug)
    {
        $query = $this->getEntityManager()
            ->createQuery('SELECT c.id as id, c.name as name FROM ' . self::ENTITY . ' c
                               WHERE c.slug = :slug ')
            ->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true)
            ->setParameter('slug', $slug)
        ;

        return $query->getOneOrNullResult();
    }

    /**
     * This function is used to get all teams by category id and teamdates
     *
     * @param string $teamId date
     * @param string $playerId
     * @return array
     */
    public function findAllCategories($teamId = null , $playerId = null)
    {
        $builder =
            $this->getEntityManager()->createQueryBuilder()
                ->select('c', 's', 't', 'pt', 'p')
                ->from(self::ENTITY, 'c')
                ->leftJoin('c.season', 's')
                ->leftJoin('s.team', 't')
                ->leftJoin('t.playerTeams', 'pt')
                ->leftJoin('pt.player', 'p')
        ;

        // check category id
        if($teamId){
            $builder
                ->andWhere('t.id = :teamId')
                ->setParameter('teamId', $teamId);
        }

        // check playerId
        if($playerId){
            $builder
                ->andWhere('p.id = :plId')
                ->setParameter('plId', $playerId);
        }

        $builder
            ->groupBy('c.name')
            ->orderBy('c.position')
        ;

        $builder->getQuery()->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true);

        return $builder->getQuery()->getResult();
    }

}
