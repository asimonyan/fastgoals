<?php
/**
 * Created by PhpStorm.
 * User: aram
 * Date: 6/21/15
 * Time: 11:55 AM
 */

namespace AppBundle\Entity;

use AppBundle\Traits\FileInfo;
use AppBundle\Traits\ListPhoto;
use AppBundle\Traits\RightPhoto;
use AppBundle\Traits\SocialPhoto;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Context\ExecutionContext;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Class TopPost
 * @package AppBundle\Entity
 *
 * @ORM\Table(name="post")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\PostRepository")
 *
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="class_name", type="string")
 * @ORM\DiscriminatorMap({"post" = "Post",
 *                        "topPost" = "TopPost",
 *                        "mainPost" = "MainPost"})
 *  @Assert\Callback(methods={"checkVideoUrl"}, groups={"Default"})
 */
abstract class Post
{
    use FileInfo, ListPhoto, RightPhoto, SocialPhoto;

    const PUBLISH = true;
    const UN_PUBLISH = false;
    const LIST_IMAGE = 0;
    const RIGHT_IMAGE = 1;
    const SOCIAL_IMAGE = 2;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @Groups({"posts"})
     */
    protected  $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    protected $title;

    /**
     * @var string slug
     *
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(type="string", length=255, unique=true, nullable=false)
     * @Groups({"main"})
     */
    protected $slug;

    /**
     * @var datetime $posts_date
     * @ORM\Column(name="posts_date", type="date")
     * @Groups({"posts"})
     */
    protected $postDate;

    /**
     * @var string
     *
     * @ORM\Column(name="video_url", type="string", length=255, nullable=true)
     * @Groups({"posts"})
     */
    protected $video;

    /**
     * @var string
     *
     * @ORM\Column(name="video_duration", type="string", length=20)
     * @Groups({"posts"})
     */
    protected $videoDuration;


    /**
     * @var string
     *
     * @ORM\Column(name="publish", type="boolean")
     */
    protected $publish = self::UN_PUBLISH;

    /**
     * @var string
     *
     * @ORM\Column(name="video_wire", type="string", length=500,  nullable=true)
     * @Groups({"posts"})
     */
    protected $videoPlayWire;

    /**
     * @var integer $meta_desc
     * @ORM\Column(name="meta_desc", type="string", length=300, nullable=true)
     */
    protected $meta_desc;

    /**
     * @var integer $meta_kw
     * @ORM\Column(name="meta_kw", type="string", length=200, nullable=true)
     */
    protected $meta_kw;

    /**
     * @var integer
     *
     * @ORM\Column(name="views", type="integer")
     */
    private $views = 0;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="created", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    protected $created;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="updated", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    protected $updated;

    /**
     * @var
     * @ORM\Column(name="position", type="integer", nullable=true)
     */
    protected $position;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Post
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Post
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Post
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Post
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set postDate
     *
     * @param \DateTime $postDate
     * @return Post
     */
    public function setPostDate($postDate)
    {
        $this->postDate = $postDate;

        return $this;
    }

    /**
     * Get postDate
     *
     * @return \DateTime 
     */
    public function getPostDate()
    {
        return $this->postDate;
    }

    /**
     * Set video
     *
     * @param string $video
     * @return Post
     */
    public function setVideo($video)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * Get video
     *
     * @return string 
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Set videoDuration
     *
     * @param string $videoDuration
     * @return Post
     */
    public function setVideoDuration($videoDuration)
    {
        $this->videoDuration = $videoDuration;

        return $this;
    }

    /**
     * Get videoDuration
     *
     * @return string 
     */
    public function getVideoDuration()
    {
        return $this->videoDuration;
    }

    /**
     * Set meta_desc
     *
     * @param string $metaDesc
     * @return Post
     */
    public function setMetaDesc($metaDesc)
    {
        $this->meta_desc = $metaDesc;

        return $this;
    }

    /**
     * Get meta_desc
     *
     * @return string 
     */
    public function getMetaDesc()
    {
        return $this->meta_desc;
    }

    /**
     * Set meta_kw
     *
     * @param string $metaKw
     * @return Post
     */
    public function setMetaKw($metaKw)
    {
        $this->meta_kw = $metaKw;

        return $this;
    }

    /**
     * Get meta_kw
     *
     * @return string 
     */
    public function getMetaKw()
    {
        return $this->meta_kw;
    }

    /**
     * Set views
     *
     * @param integer $views
     * @return Post
     */
    public function setViews($views)
    {
        $this->views = $views;

        return $this;
    }

    /**
     * Get views
     *
     * @return integer 
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * @return string
     */
    public function getShowTitle()
    {
        // get show title
        $showTitle = $this->getTitle();

        // check post class
        if($this instanceof MainPost){

            $showTitle = $this->getTeamHome()->getName() .  ' ' .
                $this->getScore() . ' ' . $this->getTeamGuest()->getName();

        }

        return $showTitle;

    }

    /**
     * Set videoPlayWireDuration
     *
     * @param string $videoPlayWire
     * @return Post
     */
    public function setVideoPlayWire($videoPlayWire)
    {
        $this->videoPlayWire = $videoPlayWire;

        return $this;
    }

    /**
     * Get videoPlayWireDuration
     *
     * @return string 
     */
    public function getVideoPlayWire()
    {
        return $this->videoPlayWire;
    }

    /**
     * @Assert\Callback
     */
    public function checkVideoUrl(ExecutionContext $context)
    {

        if($this->getVideo() && $this->getVideoPlayWire()){
            $context->addViolationAt('video', 'Grig jan ches kara 2 video urlnery miajamanak lracnes', array(), null);
            $context->addViolationAt('videoPlayWire', 'Grig jan ches kara 2 video urlnery miajamanak lracnes', array(), null);
        }

        if(!$this->getVideo() && !$this->getVideoPlayWire()){
            $context->addViolationAt('video', 'Grig jan ches kara 2 video urlnery miajamanak datark toxes', array(), null);
            $context->addViolationAt('videoPlayWire', 'Grig jan ches kara 2 video urlnery miajamanak datark toxes', array(), null);
        }

    }

    /**
     * Set publish
     *
     * @param boolean $publish
     * @return Post
     */
    public function setPublish($publish)
    {
        $this->publish = $publish;

        return $this;
    }

    /**
     * Get publish
     *
     * @return boolean 
     */
    public function getPublish()
    {
        return $this->publish;
    }


    /**
     * @ORM\PreRemove()
     */
    public function preRemove()
    {
        // get list file path
        $listPhoto = $this->getListAbsolutePath() . $this->getListPhotoName();

        // check file and remove
        if (file_exists($listPhoto)){
            unlink($listPhoto);
        }

        // get right file path
        $rightPhoto = $this->getRightAbsolutePath() . $this->getRightPhotoName();

        // check file and remove
        if (file_exists($rightPhoto)){
            unlink($rightPhoto);
        }

        // get social file path
        $socialPhoto = $this->getSocialAbsolutePath() . $this->getSocialPhotoName();

        // check file and remove
        if (file_exists($socialPhoto)){
            unlink($socialPhoto);
        }
    }

    /**
     * @ORM\PrePersist()
     */
    public function preUpload()
    {
        $this->uploadForListPhoto();
        $this->uploadForRightPhoto();
        $this->uploadForSocialPhoto();
    }


    /**
     * @param $image
     * @return mixed
     */
    public function getImageUrl($image)
    {
        $width = 267;
        $height = 0;

        $imageUrl['src'] = null ;
        $imageUrl['width'] = $width;
        $imageUrl['height'] =  $height;


        switch($image){

            case self::LIST_IMAGE:

                // check photo name
                if($this->getListPhotoName()){

                    // get path
                    $path = $this->getListAbsolutePath() . $this->getListPhotoName();

                    if(is_file($path)){
                        $imageSize = getimagesize($path);
                        $height = $width * $imageSize[1] / $imageSize[0];

                        $imageUrl['src'] = $this->getListPhotoDownloadLink() ;
                        $imageUrl['height'] =  $height;

                    }
                }
                break;
            case self::RIGHT_IMAGE:

                // check photo name
                if($this->getRightPhotoName()){

                    // get path
                    $path = $this->getRightAbsolutePath() . $this->getRightPhotoName();

                    if(is_file($path)){
                        $imageSize = getimagesize($path);
                        $height = $width * $imageSize[1] / $imageSize[0];

                        $imageUrl['src'] = $this->getRightPhotoDownloadLink() ;
                        $imageUrl['height'] =  $height;

                    }
                }
                break;
            case self::SOCIAL_IMAGE:
                // check photo name
                if($this->getSocialPhotoName()){

                    // get path
                    $path = $this->getSocialAbsolutePath() . $this->getSocialPhotoName();

                    if(is_file($path)){
                        $imageSize = getimagesize($path);
                        $height = $width * $imageSize[1] / $imageSize[0];

                        $imageUrl['src'] = $this->getSocialPhotoDownloadLink() ;
                        $imageUrl['height'] =  $height;
                    }
                }
                break;
        }

        return $imageUrl;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return Post
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }
}
