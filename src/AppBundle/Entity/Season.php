<?php
/**
 * Created by PhpStorm.
 * User: aram
 * Date: 6/25/15
 * Time: 6:06 PM
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Dates
 * @package AppBundle\Entity
 *
 * @ORM\Table(name="season")
 * @ORM\Entity
 */
class Season
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    protected $name;

    /**
     * @var string slug
     *
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(type="string", length=100, unique=true, nullable=false)
     */
    protected $slug;

    /**
     * @ORM\Column(name="start_date", type="date")
     */
    protected $startDate;

    /**
     *
     * @ORM\Column(name="end_date", type="date")
     */
    protected $endDate;

    /**
     * @ORM\ManyToMany(targetEntity="Team", mappedBy="season")
     **/
    protected $team;

    /**
     * @ORM\ManyToMany(targetEntity="TopPost", mappedBy="season")
     */
    protected  $topPost;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="season")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    protected $category;

    /**
     * @ORM\OneToMany(targetEntity="MainPost", mappedBy="season")
     * @ORM\JoinColumn(name="season_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $mainPost;

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->team = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Season
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Season
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     * @return Season
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime 
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     * @return Season
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime 
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Add team
     *
     * @param \AppBundle\Entity\Team $team
     * @return Season
     */
    public function addTeam(\AppBundle\Entity\Team $team)
    {
        $this->team[] = $team;

        return $this;
    }

    /**
     * Remove team
     *
     * @param \AppBundle\Entity\Team $team
     */
    public function removeTeam(\AppBundle\Entity\Team $team)
    {
        $this->team->removeElement($team);
    }

    /**
     * Get team
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * Set category
     *
     * @param \AppBundle\Entity\Category $category
     * @return Season
     */
    public function setCategory(\AppBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \AppBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Add mainPost
     *
     * @param \AppBundle\Entity\MainPost $mainPost
     * @return Season
     */
    public function addMainPost(\AppBundle\Entity\MainPost $mainPost)
    {
        $this->mainPost[] = $mainPost;

        return $this;
    }

    /**
     * Remove mainPost
     *
     * @param \AppBundle\Entity\MainPost $mainPost
     */
    public function removeMainPost(\AppBundle\Entity\MainPost $mainPost)
    {
        $this->mainPost->removeElement($mainPost);
    }

    /**
     * Get mainPost
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMainPost()
    {
        return $this->mainPost;
    }

    /**
     * Add topPost
     *
     * @param \AppBundle\Entity\TopPost $topPost
     * @return Season
     */
    public function addTopPost(\AppBundle\Entity\TopPost $topPost)
    {
        $this->topPost[] = $topPost;

        return $this;
    }

    /**
     * Remove topPost
     *
     * @param \AppBundle\Entity\TopPost $topPost
     */
    public function removeTopPost(\AppBundle\Entity\TopPost $topPost)
    {
        $this->topPost->removeElement($topPost);
    }

    /**
     * Get topPost
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTopPost()
    {
        return $this->topPost;
    }
}
