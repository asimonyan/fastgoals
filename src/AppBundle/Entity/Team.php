<?php
/**
 * Created by PhpStorm.
 * User: aram
 * Date: 6/25/15
 * Time: 6:10 PM
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation\Groups;

/**
 * Class Team
 * @package AppBundle\Entity
 *
 * @ORM\Table(name="team")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\TeamRepository")
 */
class Team
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"main", "posts"})
     */
    protected  $id;

    /**
     * @var string
     *
     * @Groups({"main", "posts"})
     * @ORM\Column(name="name", type="string", length=150)
     */
    protected $name;

    /**
     * @var string slug
     *
     * @Groups({"main", "posts"})
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(type="string", length=100, unique=true, nullable=false)
     */
    protected $slug;

    /**
     *
     * @ORM\OneToMany(targetEntity="Event", mappedBy="team", cascade={"all"})
     */
    protected $event;

    /**
     * @ORM\ManyToMany(targetEntity="Season", inversedBy="team")
     * @ORM\JoinTable(name="teams_season")
     **/
    protected $season;

    /**
     * @Groups({"main"})
     * @ORM\OneToMany(targetEntity="MainPost", mappedBy="teamHome")
     */
    protected $mainPostHomeTeam;

    /**
     * @Groups({"main"})
     * @ORM\OneToMany(targetEntity="MainPost", mappedBy="teamGuest")
     */
    protected $mainPostGuestTeam;

    /**
     *
     * @ORM\OneToMany(targetEntity="PlayerTeam", mappedBy="team", cascade={"all"})
     */
    protected  $playerTeams;

    /**
     * @ORM\ManyToMany(targetEntity="TopPost", mappedBy="team")
     */
    protected  $topPost;

    /**
     *
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->season = new \Doctrine\Common\Collections\ArrayCollection();
        $this->playerTeams = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Team
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Team
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Add season
     *
     * @param \AppBundle\Entity\Season $season
     * @return Team
     */
    public function addSeason(\AppBundle\Entity\Season $season)
    {
        $this->season[] = $season;

        return $this;
    }

    /**
     * Remove season
     *
     * @param \AppBundle\Entity\Season $season
     */
    public function removeSeason(\AppBundle\Entity\Season $season)
    {
        $this->season->removeElement($season);
    }

    /**
     * Get season
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSeason()
    {
        return $this->season;
    }

    /**
     * Add playerTeams
     *
     * @param \AppBundle\Entity\PlayerTeam $playerTeams
     * @return Team
     */
    public function addPlayerTeam(\AppBundle\Entity\PlayerTeam $playerTeams)
    {
        $this->playerTeams[] = $playerTeams;

        return $this;
    }

    /**
     * Remove playerTeams
     *
     * @param \AppBundle\Entity\PlayerTeam $playerTeams
     */
    public function removePlayerTeam(\AppBundle\Entity\PlayerTeam $playerTeams)
    {
        $this->playerTeams->removeElement($playerTeams);
    }

    /**
     * Get playerTeams
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPlayerTeams()
    {
        return $this->playerTeams;
    }

    /**
     * Add mainPostHomeTeam
     *
     * @param \AppBundle\Entity\MainPost $mainPostHomeTeam
     * @return Team
     */
    public function addMainPostHomeTeam(\AppBundle\Entity\MainPost $mainPostHomeTeam)
    {
        $this->mainPostHomeTeam[] = $mainPostHomeTeam;

        return $this;
    }

    /**
     * Remove mainPostHomeTeam
     *
     * @param \AppBundle\Entity\MainPost $mainPostHomeTeam
     */
    public function removeMainPostHomeTeam(\AppBundle\Entity\MainPost $mainPostHomeTeam)
    {
        $this->mainPostHomeTeam->removeElement($mainPostHomeTeam);
    }

    /**
     * Get mainPostHomeTeam
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMainPostHomeTeam()
    {
        return $this->mainPostHomeTeam;
    }

    /**
     * Add mainPostGuestTeam
     *
     * @param \AppBundle\Entity\MainPost $mainPostGuestTeam
     * @return Team
     */
    public function addMainPostGuestTeam(\AppBundle\Entity\MainPost $mainPostGuestTeam)
    {
        $this->mainPostGuestTeam[] = $mainPostGuestTeam;

        return $this;
    }

    /**
     * Remove mainPostGuestTeam
     *
     * @param \AppBundle\Entity\MainPost $mainPostGuestTeam
     */
    public function removeMainPostGuestTeam(\AppBundle\Entity\MainPost $mainPostGuestTeam)
    {
        $this->mainPostGuestTeam->removeElement($mainPostGuestTeam);
    }

    /**
     * Get mainPostGuestTeam
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMainPostGuestTeam()
    {
        return $this->mainPostGuestTeam;
    }

    /**
     * Add topPost
     *
     * @param \AppBundle\Entity\TopPost $topPost
     * @return Team
     */
    public function addTopPost(\AppBundle\Entity\TopPost $topPost)
    {
        $this->topPost[] = $topPost;

        return $this;
    }

    /**
     * Remove topPost
     *
     * @param \AppBundle\Entity\TopPost $topPost
     */
    public function removeTopPost(\AppBundle\Entity\TopPost $topPost)
    {
        $this->topPost->removeElement($topPost);
    }

    /**
     * Get topPost
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTopPost()
    {
        return $this->topPost;
    }

    /**
     * Add event
     *
     * @param \AppBundle\Entity\Event $event
     * @return Team
     */
    public function addEvent(\AppBundle\Entity\Event $event)
    {
        $this->event[] = $event;

        return $this;
    }

    /**
     * Remove event
     *
     * @param \AppBundle\Entity\Event $event
     */
    public function removeEvent(\AppBundle\Entity\Event $event)
    {
        $this->event->removeElement($event);
    }

    /**
     * Get event
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEvent()
    {
        return $this->event;
    }
}
