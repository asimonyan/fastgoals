<?php
/**
 * Created by PhpStorm.
 * User: aram
 * Date: 6/25/15
 * Time: 6:11 PM
 */


namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation\Groups;

/**
 * Class Team
 * @package AppBundle\Entity
 *
 * @ORM\Table(name="player")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\PlayerRepository")
 */
class Player
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"main", "posts", "event_player"})
     */
    protected  $id;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=100)
     * @Groups({"main", "posts", "event_player"})
     */
        protected $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=150)
     * @Groups({"main", "posts", "event_player"})
     */
    protected $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="short_name", type="string", length=50, nullable=true)
     * @Groups({"main", "posts", "event_player"})
     */
    protected $shortName;

    /**
     * @var string slug
     *
     * @Gedmo\Slug(fields={"firstName", "lastName"})
     * @ORM\Column(type="string", length=100, unique=true, nullable=false)
     * @Groups({"main", "posts", "event_player"})
     */
    protected $slug;

    /**
     *
     * @ORM\OneToMany(targetEntity="Event", mappedBy="player", cascade={"all"})
     */
    protected $event;


    /**
     * @ORM\OneToMany(targetEntity="PlayerTeam", mappedBy="player", cascade={"all"})
     */
    protected $playerTeams;

    /**
     * @ORM\ManyToMany(targetEntity="TopPost", mappedBy="player")
     */
    protected $topPost;

    /**
     *
     * @return string
     */
    public function __toString()
    {
        return $this->firstName . ' ' .$this->lastName;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->playerTeams = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return Player
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return Player
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set shortName
     *
     * @param string $shortName
     * @return Player
     */
    public function setShortName($shortName)
    {
        $this->shortName = $shortName;

        return $this;
    }

    /**
     * Get shortName
     *
     * @return string 
     */
    public function getShortName()
    {
        return $this->shortName;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Player
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Add playerTeams
     *
     * @param \AppBundle\Entity\PlayerTeam $playerTeams
     * @return Player
     */
    public function addPlayerTeam(\AppBundle\Entity\PlayerTeam $playerTeams)
    {
        $this->playerTeams[] = $playerTeams;
        $playerTeams->setPlayer($this);

        return $this;
    }

    /**
     * Remove playerTeams
     *
     * @param \AppBundle\Entity\PlayerTeam $playerTeams
     */
    public function removePlayerTeam(\AppBundle\Entity\PlayerTeam $playerTeams)
    {
        $this->playerTeams->removeElement($playerTeams);
    }

    /**
     * Get playerTeams
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPlayerTeams()
    {
        return $this->playerTeams;
    }

    /**
     * Add topPost
     *
     * @param \AppBundle\Entity\TopPost $topPost
     * @return Player
     */
    public function addTopPost(\AppBundle\Entity\TopPost $topPost)
    {
        $this->topPost[] = $topPost;

        return $this;
    }

    /**
     * Remove topPost
     *
     * @param \AppBundle\Entity\TopPost $topPost
     */
    public function removeTopPost(\AppBundle\Entity\TopPost $topPost)
    {
        $this->topPost->removeElement($topPost);
    }

    /**
     * Get topPost
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTopPost()
    {
        return $this->topPost;
    }

    /**
     * Add event
     *
     * @param \AppBundle\Entity\Event $event
     * @return Player
     */
    public function addEvent(\AppBundle\Entity\Event $event)
    {
        $this->event[] = $event;

        return $this;
    }

    /**
     * Remove event
     *
     * @param \AppBundle\Entity\Event $event
     */
    public function removeEvent(\AppBundle\Entity\Event $event)
    {
        $this->event->removeElement($event);
    }

    /**
     * Get event
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEvent()
    {
        return $this->event;
    }


    /**
     * This function is used to g et player number
     *
     * @param $teamId
     * @return null
     */
    public function getNumberByTeam($teamId)
    {
        // get player teams
        $playerTeams = $this->getPlayerTeams();

        // check player
        if($playerTeams){

            // loop for players
            foreach($playerTeams as $playerTeam){

                // get team
                $team = $playerTeam->getTeam();

                // check team id
                if($team->getId() == $teamId){

                    // return number
                    return $playerTeam->getNumber();
                }
            }
        }

        return null;
    }

}
