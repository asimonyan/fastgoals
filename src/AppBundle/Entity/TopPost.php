<?php
/**
 * Created by PhpStorm.
 * User: aram
 * Date: 6/24/15
 * Time: 12:08 AM
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation\Groups;


/**
 * Class TopPost
 * @package AppBundle\Entity
 *
 * @ORM\Table(name="top_post")
 * @ORM\Entity()
 */
class TopPost extends Post
{
    /**
     * @Groups({"main"})
     * @ORM\ManyToMany(targetEntity="Team", inversedBy="topPost")
     * @ORM\JoinTable(name="top_post_teams",
     *      joinColumns={@ORM\JoinColumn(name="top_post_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="team_id", referencedColumnName="id")}
     *      )
     **/
    protected  $team;

    /**
     * @Groups({"main"})
     * @ORM\ManyToMany(targetEntity="Player", inversedBy="topPost")
     * @ORM\JoinTable(name="top_post_players",
     *      joinColumns={@ORM\JoinColumn(name="top_post_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="player_id", referencedColumnName="id")}
     *      )
     **/
    protected $player;

    /**
     * @Groups({"main"})
     * @ORM\ManyToMany(targetEntity="Season", inversedBy="topPost")
     * @ORM\JoinTable(name="top_post_seasons",
     *      joinColumns={@ORM\JoinColumn(name="top_post_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="season_id", referencedColumnName="id")}
     *      )
     **/
    protected $season;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->team = new \Doctrine\Common\Collections\ArrayCollection();
        $this->player = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add team
     *
     * @param \AppBundle\Entity\Team $team
     * @return TopPost
     */
    public function addTeam(\AppBundle\Entity\Team $team)
    {
        $this->team[] = $team;

        return $this;
    }

    /**
     * Remove team
     *
     * @param \AppBundle\Entity\Team $team
     */
    public function removeTeam(\AppBundle\Entity\Team $team)
    {
        $this->team->removeElement($team);
    }

    /**
     * Get team
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * Add player
     *
     * @param \AppBundle\Entity\Player $player
     * @return TopPost
     */
    public function addPlayer(\AppBundle\Entity\Player $player)
    {
        $this->player[] = $player;

        return $this;
    }

    /**
     * Remove player
     *
     * @param \AppBundle\Entity\Player $player
     */
    public function removePlayer(\AppBundle\Entity\Player $player)
    {
        $this->player->removeElement($player);
    }

    /**
     * Get player
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPlayer()
    {
        return $this->player;
    }

    /**
     * Add season
     *
     * @param \AppBundle\Entity\Season $season
     * @return TopPost
     */
    public function addSeason(\AppBundle\Entity\Season $season)
    {
        $this->season[] = $season;

        return $this;
    }

    /**
     * Remove season
     *
     * @param \AppBundle\Entity\Season $season
     */
    public function removeSeason(\AppBundle\Entity\Season $season)
    {
        $this->season->removeElement($season);
    }

    /**
     * Get season
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSeason()
    {
        return $this->season;
    }
}
