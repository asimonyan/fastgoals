<?php
/**
 * Created by PhpStorm.
 * User: aram
 * Date: 6/25/15
 * Time: 6:11 PM
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation\Groups;


/**
 * Class Team
 * @package AppBundle\Entity
 *
 * @ORM\Table(name="event")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\EventRepository")
 */
class Event
{
    const EVENT_TYPE_GOAL = 1;
    const EVENT_TYPE_YELLOW_CARD = 2;
    const EVENT_TYPE_RED_CARD = 3;
    const EVENT_TYPE_SUBSTITUDE_INGOING = 4;
    const EVENT_TYPE_SUBSTITUDE_OUT = 5;
    const EVENT_TYPE_LINE_UPS = 6;

    /**
     * for normal goal
     */
    const EVENT_GOAL = 0;

    /**
     * for own goal
     */
    const EVENT_OWN_GOAL = 1;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected  $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="smallint")
     * @Groups({"main", "posts", "event_player"})
     */
    protected $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="own", type="smallint", options={"default" = 0})
     * @Groups({"main", "posts", "event_player"})
     */
    protected $own = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="minute", type="smallint")
     * @Groups({"main", "posts", "event_player"})
     */
    protected $minute = 0;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Team", inversedBy="event")
     * @ORM\JoinColumn(name="team_id", referencedColumnName="id", onDelete="SET NULL")
     * @Groups({"main", "posts"})
     */
    protected  $team;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Player", inversedBy="event")
     * @ORM\JoinColumn(name="player_id", referencedColumnName="id", onDelete="SET NULL")
     * @Groups({"main", "posts"})
     */
    protected  $player;

    /**
     *
     * @ORM\ManyToOne(targetEntity="MainPost", inversedBy="event", cascade={"persist"})
     * @ORM\JoinColumn(name="post_id", referencedColumnName="id", onDelete="SET NULL")
     * @Groups({"main"})
     */
    protected $post;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Event
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set own
     *
     * @param integer $own
     * @return Event
     */
    public function setOwn($own)
    {
        $this->own = $own;

        return $this;
    }

    /**
     * Get own
     *
     * @return integer 
     */
    public function getOwn()
    {
        return $this->own;
    }

    /**
     * Set minute
     *
     * @param integer $minute
     * @return Event
     */
    public function setMinute($minute)
    {
        $this->minute = $minute;

        return $this;
    }

    /**
     * Get minute
     *
     * @return integer 
     */
    public function getMinute()
    {
        return $this->minute;
    }

    /**
     * Set team
     *
     * @param \AppBundle\Entity\Team $team
     * @return Event
     */
    public function setTeam(\AppBundle\Entity\Team $team = null)
    {
        $this->team = $team;
        $team->addEvent($this);

        return $this;
    }

    /**
     * Get team
     *
     * @return \AppBundle\Entity\Team 
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * Set player
     *
     * @param \AppBundle\Entity\Player $player
     * @return Event
     */
    public function setPlayer(\AppBundle\Entity\Player $player = null)
    {
        $this->player = $player;
        $player->addEvent($this);

        return $this;
    }

    /**
     * Get player
     *
     * @return \AppBundle\Entity\Player 
     */
    public function getPlayer()
    {
        return $this->player;
    }

    /**
     * Set post
     *
     * @param \AppBundle\Entity\MainPost $post
     * @return Event
     */
    public function setPost(\AppBundle\Entity\MainPost $post = null)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get post
     *
     * @return \AppBundle\Entity\MainPost 
     */
    public function getPost()
    {
        return $this->post;
    }
}
