<?php
/**
 * Created by PhpStorm.
 * User: aram
 * Date: 7/12/15
 * Time: 1:26 PM
 */

namespace AppBundle\Controller\Rest;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Doctrine\ORM\Mapping as ORM;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 *
 * @Rest\RouteResource("Player")
 * @Rest\Prefix("/api")
 * @Rest\NamePrefix("rest_")
 */
class RestPlayerController extends FOSRestController
{

    /**
     *
     * This function is used to get a all items (posts)
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Items",
     *  description="This function is used to get a all items (posts)",
     *  statusCodes={
     *         200="Returned when successful",
     *         404="Returned when the Team is not found"
     *     }
     * )
     *
     * @Rest\View()
     */
    public function searchAction(Request $request)
    {
        // get fast goal service
        $fastGoalsService = $this->get('fast_goal');

        // get search query
        $term = $request->get('term');

        // get league id
        $leagueId = $request->get("leagueId");

        // get team id
        $teamId = $request->get("teamId");

        // get date from request
        $d = $request->get("date");

        // date id
        $date = $fastGoalsService->getStartAndEndDates($d);

        // get entity manager
        $em = $this->getDoctrine()->getManager();

        // find by player
        $players = $em->getRepository('AppBundle:Player')
            ->findAllPlayerBySearch($term , $leagueId , $teamId, $date);

        $resultPlayers = array();

        foreach($players as $player) {
            $name = $player['fName'] . " " . $player['lName'];
            $resultPlayers[] = array(
                "id" => $player['slug'],
                "value" => $name,
                "label" => $name
            );
        }
        return $resultPlayers;
    }
}