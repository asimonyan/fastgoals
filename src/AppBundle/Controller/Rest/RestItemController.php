<?php
/**
 * Created by PhpStorm.
 * User: aram
 * Date: 7/12/15
 * Time: 1:26 PM
 */

namespace AppBundle\Controller\Rest;

use AppBundle\Entity\Ad;
use AppBundle\Entity\MainPost;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Doctrine\ORM\Mapping as ORM;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 *
 * @Rest\RouteResource("Item")
 * @Rest\Prefix("/api")
 * @Rest\NamePrefix("rest_")
 */
class RestItemController extends FOSRestController
{
    /**
     * This function is used to get all Posts and events by Team and post.
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Post",
     *  description="This function is used to get all Players and events by Team",
     *  statusCodes={
     *         200="Returned when successful",
     *         404="Returned when the Player not found"
     *     }
     * )
     *
     * @Rest\View(serializerGroups={"main"})
     */
    public function cgetTeamPostAction($team, $post)
    {
        $em = $this->getDoctrine()->getManager();
        $players = $em->getRepository('AppBundle:Event')->findAllByTeamAndPost($team, $post);

        return $players; // or return it in a Response
    }

    /**
     *
     * This function is used to get a all items (posts)
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Items",
     *  description="This function is used to get a all items (posts)",
     *  statusCodes={
     *         200="Returned when successful",
     *         404="Returned when the Team is not found"
     *     }
     * )
     *
     * @Rest\View()
     */
    public function cgetAction(Request $request)
    {
        // get fast goal service
        $fastGoalsService = $this->get('fast_goal');

        $offset = $request->get("offset");
        $limit = $request->get("limit");

        // league id
        $l = $request->get("l");
        // team id
        $t = $request->get("t");
        // newest
        $n = $request->get("n");
        // player id
        $p = $request->get("p");
        // date id
        $d = $fastGoalsService->getStartAndEndDates($request->get("d"));

        // get most
        $m = $request->get("m");

        // get items
        $items = $fastGoalsService->getItems($offset, $limit, $l, $t, $n, $p, $d, $m);

        return $items;


    }

    /**
     * This function is used to get a all items (posts)
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Items",
     *  description="This function is used to get a all items (posts)",
     *  statusCodes={
     *         200="Returned when successful",
     *         404="Returned when the Team is not found"
     *     }
     * )
     *
     * @Rest\View()
     */
    public function updateViewsAction(Request $request)
    {
        $postSlug = $request->get('postSlug');

        $em = $this->getDoctrine()->getManager();
        $em->getRepository('AppBundle:Post')->updateOneBySlug($postSlug);

        return Codes::HTTP_OK;
    }
}