<?php
/**
 * Created by PhpStorm.
 * User: aram
 * Date: 7/7/15
 * Time: 7:52 PM
 */

namespace AppBundle\Controller\Rest;

use AppBundle\Entity\Team;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Doctrine\ORM\Mapping as ORM;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 *
 * @Rest\RouteResource("Team")
 * @Rest\Prefix("/api")
 * @Rest\NamePrefix("rest_")
 */
class RestTeamController extends FOSRestController
{
    const ENTITY = 'AppMainBundle:Team';


    /**
     *
     * This function is used to get a Team by given id.
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Team",
     *  description="This function is used to get a Team by given id",
     *  statusCodes={
     *         200="Returned when successful",
     *         404="Returned when the Team is not found"
     *     }
     * )
     *
     * @Rest\View()
     *
     */
    public function searchAction(Request $request)
    {
        // get fast goal service
        $fastGoalsService = $this->get('fast_goal');

        // get search query
        $term = $request->get('term');

        // get league id
        $leagueId = $request->get("leagueId");

        // get players
        $playerId = $request->get("playerId");

        // get date from request
        $d = $request->get("date");

        // date id
        $date = $fastGoalsService->getStartAndEndDates($d);

        // get entity manager
        $em = $this->getDoctrine()->getManager();

        // find teams
        $teams = $em->getRepository('AppBundle:Team')
            ->findAllTeamsBySearch($term, $leagueId, $playerId, $date);

        $resultTeams = array();

        foreach($teams as $team) {
            $name = $team['name'];
            $resultTeams[] = array(
                "id" => $team['slug'],
                "value" => $name,
                "label" => $name
            );
        }
        return $resultTeams;
    }

    /**
     *
     * This function is used to get a Team by given id.
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Team",
     *  description="This function is used to get a Team by given id",
     *  statusCodes={
     *         200="Returned when successful",
     *         404="Returned when the Team is not found"
     *     }
     * )
     *
     * @Rest\View(serializerGroups={"main"})
     * @ParamConverter("team", class="AppMainBundle:Team")
     *
     */
    public function getAction(Team $team)
    {
        return $team;
    }

    /**
     * This function is used to get all Teams.
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Team",
     *  description="This function is used to get all Teams",
     *  statusCodes={
     *         200="Returned when successful",
     *         404="Returned when the Teams is empty"
     *     }
     * )
     *
     * @return mixed
     * @Rest\View(serializerGroups={"main"})
     */
    public function cgetAction()
    {
        $em = $this->getDoctrine()->getManager();
        $teams = $em->getRepository(self::ENTITY)->findAll();

        return $teams;
    }

    /**
     * This function is used to get all Players by Team.
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Team",
     *  description="This function is used to get all Players by Team",
     *  statusCodes={
     *         200="Returned when successful",
     *         404="Returned when the Player not found"
     *     }
     * )
     *
     * @return mixed
     * @param $team
     * @param $status
     * @Rest\View(serializerGroups={"main"})
     */

    public function cgetStatusAction($team, $status)
    {
        $em = $this->getDoctrine()->getManager();
        $teams = $em->getRepository('AppBundle:PlayerTeam')->findAllByTeam($team, $status);

        $teams['wtf'] = '';

        return $teams;
    }

    /**
     * This function is used to get all Players by Team.
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Team",
     *  description="This function is used to get all Players by Team",
     *  statusCodes={
     *         200="Returned when successful",
     *         404="Returned when the Player not found"
     *     }
     * )
     *
     * @return mixed
     * @Rest\View(serializerGroups={"main"})
     * @param $team
     */
    public function cgetPlayersAction($team)
    {
        $em = $this->getDoctrine()->getManager();
        $players = $em->getRepository('FastgoalsMainBundle:Players')->findAllByTeam($team);

        return $players;
    }

}
