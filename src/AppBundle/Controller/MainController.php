<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Event;
use AppBundle\Entity\MainPost;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class MainController
 * @package AppBundle\Controller
 */
class MainController extends Controller
{
    const MOST_POPULAR = 0;
    const TOP_POST = 1;

    /**
     * @Route("/page/{page}/", name="page")
     * @Template()
     */
    public function pageAction($page)
    {
        // get fast goal service
        $fastGoalsService = $this->get('fast_goal');

        // get entity manager
        $em = $this->getDoctrine()->getManager();

        // find all categories
        $cats = $em->getRepository('AppBundle:Category')->findAllCategories();

        // find all players
        $players = $em->getRepository('AppBundle:Player')->findAllPlayers();

        // find all teams
        $teams = $em->getRepository('AppBundle:Team')->findAllTeams();

        $currentYearMonths = $fastGoalsService->getLastTwelveMonths();
        $lastWeekDates = $fastGoalsService->getLastWeekDates();
        $lastMonthDates = $fastGoalsService->getLastMonthDates();

        // find new page
        $newPage = $em->getRepository("AppBundle:Menu")->findOneBySlug($page);

        if(!$newPage){
            throw $this->createNotFoundException('page note found');
        }

        return array(
            'dateSlug' => self::DATE,
            'categorySlug' => self::CATEGORY,
            'teamSlug' => self::TEAM,
            'playerSlug' => self::PLAYER,
            'page' => $newPage,
            'categories' => $cats,
            'players' => $players,
            'teams' => $teams,
            'currentYearMonths' => $currentYearMonths,
            'lastWeekDates' => $lastWeekDates,
            'lastMonthDates' => $lastMonthDates,
            'most' => self::MOST,
            'categoryId' => null,
            'teamId' => null,
            'playerId' => null,
            'dateSlugView' => $fastGoalsService->getDateText(self::DATE),
        );
    }

    // constant for params
    const DATE = 'date';
    const CATEGORY = 'league';
    const TEAM = 'team';
    const PLAYER = 'player';
    const MOST = 'most';

    /**
     * @Route("/post/{slug}/", name="post_detail")
     * @Template()
     */
    public function postDetailAction(Request $request, $slug)
    {
        $homeTeamId = null;
        $guestTeamId = null;

        // get fast goal service
        $fastGoalsService = $this->get('fast_goal');

        // get entity manager
        $em = $this->getDoctrine()->getManager();

        // find post by slug
        $post = $em->getRepository('AppBundle:Post')->findPostSlug($slug);

        // new response
        $response = new Response();

        // set last modified data
        $response->setLastModified($post->getUpdated());

        // Set response as public. Otherwise it will be private by default.
        $response->setPublic();

        // Check that the Response is not modified for the given Request
        if ($response->isNotModified($request)) {

            // return the 304 Response immediately
            return $response;
        }

        // find events
        $events = $em->getRepository('AppBundle:Event')->findOneByPlayers($slug);

        // find all categories
        $cats = $em->getRepository('AppBundle:Category')->findAllCategories();

        // find all players
        $players = $em->getRepository('AppBundle:Player')->findAllPlayers();

        // find all teams
        $teams = $em->getRepository('AppBundle:Team')->findAllTeams();

        if($post instanceof MainPost){

            // find right post for main post
            $rightPost = $em->getRepository('AppBundle:Post')->findByRightPosts($post);
        }
        else{

            // find right post for top post
            $rightPost = $em->getRepository('AppBundle:Post')->findByRightPostsForTop($post);
        }



        // get ad for right block
        $rightAd = $em->getRepository('AppBundle:Ad')->findOneRight();

        // check post , and return exception if null
        if(!$post){

            throw $this->createNotFoundException('post not found');
        }

        if($post instanceof MainPost){


            $lineupPlayers = array(
                $post->getTeamHome()->getId() => array(),
                $post->getTeamGuest()->getId() => array()
            );

            $substitutePlayers = array(
                $post->getTeamHome()->getId() => array(),
                $post->getTeamGuest()->getId() => array()
            );

            $homeTeamId = $post->getTeamHome()->getId();
            $guestTeamId = $post->getTeamGuest()->getId();

            $playerEvents = array();

            foreach ($events as $event) {
                $playerId = $event->getPlayer()->getId();
                $eventType = $event->getType();

                if (!isset($playerEvents[$playerId])) {
                    $playerEvents[$playerId] = array();
                }
                if ($eventType != Event::EVENT_TYPE_SUBSTITUDE_INGOING) {

                    if($eventType == Event::EVENT_TYPE_GOAL && $event->getOwn() == Event::EVENT_OWN_GOAL){
                        $playerEvents[$playerId][$eventType][] = array('own' => $event->getMinute());
                    }
                    else{
                        $playerEvents[$playerId][$eventType][] = $event->getMinute();
                    }
                }
            }

            foreach ($events as $event) {
                $teamId = $event->getTeam()->getId();
                $playerId = $event->getPlayer()->getId();

                if ($event->getType() == Event::EVENT_TYPE_SUBSTITUDE_INGOING) {
                    if (!isset($substitutePlayers[$teamId][$playerId])) {
                        $substitutePlayers[$teamId][$playerId] = array(
                            "substituted" => $event->getMinute(),
                            "number" => $event->getPlayer()->getNumberByTeam($teamId),
                            "slug" => $event->getPlayer()->getSlug(),
                            "name" => $event->getPlayer()->getFirstName() . " " . $event->getPlayer()->getLastName()
                        );
                    }

                    $substitutePlayers[$teamId][$playerId]["events"] = $playerEvents[$playerId];
                }
            }

            foreach ($events as $event) {
                $teamId = $event->getTeam()->getId();
                $playerId = $event->getPlayer()->getId();

                if ($event->getType() != Event::EVENT_TYPE_SUBSTITUDE_INGOING && !isset($substitutePlayers[$teamId][$playerId])) {
                    if (!isset($lineupPlayers[$teamId][$playerId])) {
                        $lineupPlayers[$teamId][$playerId] = array(
                            "substituted" => false,
                            "number" => $event->getPlayer()->getNumberByTeam($teamId),
                            "slug" => $event->getPlayer()->getSlug(),
                            "name" => $event->getPlayer()->getFirstName() . " " . $event->getPlayer()->getLastName(),
                            "events" => array()
                        );

                        $lineupPlayers[$teamId][$playerId]["events"] = $playerEvents[$playerId];
                    }

                    if ($event->getType() == Event::EVENT_TYPE_SUBSTITUDE_OUT) {
                        $lineupPlayers[$teamId][$playerId]["substituted"] = $event->getMinute();
                    }
                }
            }
        }

        $currentYearMonths = $fastGoalsService->getLastTwelveMonths();
        $lastWeekDates = $fastGoalsService->getLastWeekDates();
        $lastMonthDates = $fastGoalsService->getLastMonthDates();

        $post->setViews($post->getViews() + 1 );
        $em->persist($post);
        $em->flush();

       $returnResult =  array(
            'homeTeamId'=> $homeTeamId,
            'guestTeamId'=> $guestTeamId,
            'page' => $post,
            'rightPosts' => $rightPost,
            'categories' => $cats,
            'players' => $players,
            'lineupPlayers' => isset($lineupPlayers) ? $lineupPlayers : null,
            'substitutePlayers' => isset($substitutePlayers) ? $substitutePlayers : null,
            'dateSlug' => self::DATE,
            'dateSlugView' => $fastGoalsService->getDateText(self::DATE),
            'categorySlug' => self::CATEGORY,
            'teamSlug' => self::TEAM,
            'playerSlug' => self::PLAYER,
            'categoryId' => null,
            'teamId' => null,
            'playerId' => null,
            'teams' => $teams,
            'currentYearMonths' => $currentYearMonths,
            'lastWeekDates' => $lastWeekDates,
            'lastMonthDates' => $lastMonthDates,
            'ad'=> $rightAd,
            'most' => self::MOST
        );

        return $this->render('AppBundle:Main:postDetail.html.twig',
            $returnResult,
            $response
        );

    }


    /**
     * @Route("/{dateSlug}/{categorySlug}/{teamSlug}/{playerSlug}/{most}" ,
     *          defaults={"dateSlug"="date", "categorySlug"="league", "teamSlug"="team", "playerSlug"="player", "most"="most"},
     *          name="homepage")
     * @Template()
     */
    public function indexAction($dateSlug = self::DATE, $categorySlug = self::CATEGORY, $teamSlug = self::TEAM, $playerSlug = self::PLAYER, $most = self::MOST )
    {
        // get fast goal service
        $fastGoalsService = $this->get('fast_goal');

        // get entity manager
        $em = $this->getDoctrine()->getManager();

        // empty values
        $categoryId = null;
        $categoryName = null;
        $teamId = null;
        $teamName = null;
        $playerId = null;
        $playerName = null;
        $playerLastName = null;
        $startEndDate = null;



        // check category slug
        if ($categorySlug != self::CATEGORY) {

            // get category`s name and id by slug
            $categories = $em->getRepository('AppBundle:Category')->findNameAndIdBySlug($categorySlug);

            // check category
            if($categories){

                $categoryId = $categories['id'];
                $categoryName = $categories['name'];
            }
        }

        // check team slug
        if ($teamSlug != self::TEAM) {

            // get team`s name and id by slug
            $team = $em->getRepository('AppBundle:Team')->findNameAndIdBySlug($teamSlug);

            // check team
            if($team){

                $teamId = $team['id'];
                $teamName = $team['name'];
            }
        }

        // check player slug
        if ($playerSlug != self::PLAYER ) {

            // het player`s name, lastNme, and is by slug
            $player = $em->getRepository('AppBundle:Player')->findFullNameAndIdBySlug($playerSlug);

            // check player
            if($player){

                $playerId = $player['id'];
                $playerName = $player['fName'];
                $playerLastName = $player['lName'];
            }
        }

        // check date slug
        if ($dateSlug != self::DATE) {

            // get start and end date of filter
            $startEndDate = $fastGoalsService->getStartAndEndDates($dateSlug);
        }

        // get all categories
        $categories = $em->getRepository('AppBundle:Category')->findAllCategories($teamId, $playerId);

        // get all players
        $players = $em->getRepository('AppBundle:Player')
            ->findAllPlayers($categoryId, $teamId, $startEndDate);

        // get all teams
        $teams = $em->getRepository('AppBundle:Team')
            ->findAllTeams($categoryId, $playerId, $startEndDate);

        $currentYearMonths = $fastGoalsService->getLastTwelveMonths();
        $lastWeekDates = $fastGoalsService->getLastWeekDates();
        $lastMonthDates = $fastGoalsService->getLastMonthDates();


        // get fast goal service
        $fastGoalsService = $this->get('fast_goal');


        return array(
            'categories' => $categories,
            'players' => $players,
            'teams' => $teams,
            'dateSlug' => $dateSlug,
            'dateSlugView' => $fastGoalsService->getDateText($dateSlug),
            'categorySlug' => $categorySlug,
            'teamSlug' => $teamSlug,
            'playerSlug' => $playerSlug,
            'categoryId' => $categoryId,
            'categoryName' =>$categoryName,
            'teamId' => $teamId,
            'teamName' => $teamName,
            'playerId' => $playerId,
            'playerName' => $playerName,
            'playerLastName' => $playerLastName == " " || $playerLastName == null  ? null: $playerLastName,
            'most' => $most,
            'mostName' => $most == self::MOST_POPULAR ? "Most Popular" : "Best Moments" ,
            'currentYearMonths' => $currentYearMonths,
            'lastWeekDates' => $lastWeekDates,
            'lastMonthDates' => $lastMonthDates
        );
    }
}
