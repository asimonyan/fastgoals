<?php
/**
 * Created by PhpStorm.
 * User: aram
 * Date: 7/28/15
 * Time: 10:58 AM
 */

namespace AppBundle\Filters;

use AppBundle\Entity\Post;
use Doctrine\ORM\Mapping\ClassMetaData;
use Doctrine\ORM\Query\Filter\SQLFilter;


/**
 * Class PublishFilter
 * @package AppBundle\Filters
 */
class PublishFilter extends SQLFilter
{

    /**
     * @param ClassMetaData $targetEntity
     * @param string $targetTableAlias
     * @return string
     */
    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias)
    {
        // check name
        $name = $targetEntity->reflClass->getName();

        // filter for child entities
        if ($name == 'AppBundle\Entity\Post' || $name == 'AppBundle\Entity\MainPost' || $name == 'AppBundle\Entity\TopPost') {

            // show only child with active status
            return $targetTableAlias.'.publish = ' . Post::PUBLISH;
        }
        else {

            return '';
        }
    }
}