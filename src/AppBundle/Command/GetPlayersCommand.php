<?php
/**
 * Created by PhpStorm.
 * User: aram
 * Date: 7/29/15
 * Time: 6:43 PM
 */

namespace AppBundle\Command;

use AppBundle\Entity\Player;
use AppBundle\Entity\PlayerTeam;
use Doctrine\ORM\PersistentCollection;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use \PHPExcelReader\SpreadsheetReader as Reader;


class GetPlayersCommand extends ContainerAwareCommand
{

    /**
     *
     */
    protected function configure()
    {
        $this
            ->setName('fastgoal:player:integrate')
            ->setDescription('integrate from excel to database')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $fileName = __DIR__ . "/players.xls";

        $em = $this->getContainer()->get("doctrine")->getManager();

        $output->writeln("<info>Starting...</info>");

//        $em->createQuery("DELETE FROM AppBundle:PlayerTeam")->execute();
//        $em->createQuery("DELETE FROM AppBundle:Player")->execute();

        $data = new Reader($fileName);

        $i = 2;
        while ($data->val($i, 1) != '')
        {
            $teamId = $data->val($i, 5);
            $teamName = $data->val($i, 1);

            $team = $em->getRepository("AppBundle:Team")->find($teamId);

            if($team){

                if(trim(strtoupper($team->getName()))!= trim(strtoupper($teamName))){

                    $output->writeln("<info>$teamName team name is different</info>");
//                        exit;
                }

                $firstName = (string)$data->val($i, 3);
                $lastName = (string)$data->val($i, 4);

//                if($firstName != '' && $lastName != ''){
                if(($firstName == '' && $lastName != '') || $firstName != '' && $lastName == ''){

                    $player = new Player();
                    $firstName = ucfirst($firstName);

                    $newLastName = null;

                    $lastName = trim($lastName);

                    $lastName = explode(' ', $lastName);
                    if(count($lastName) > 0){

                        foreach($lastName as $name){
                            if($name == end($lastName)){
                                $newLastName .= " " . ucfirst($name);
                            }
                            else{
                                $newLastName .= " " .  strtolower($name);
                            }
                        }
                    }
                    else{
                        // explode
                        $lastName = explode('-', $lastName);

                        if(count($lastName) > 0){

                            foreach($lastName as $name){

                                if($name == end($lastName)){
                                    $newLastName .= ucfirst($name);
                                }
                                else{
                                    $newLastName .= ucfirst($name) . "-";
                                }
                            }
                        }
                    }

                    if($lastName == null){
                        $newLastName .= ucfirst($lastName);
                    }

                    $player->setFirstName($firstName);
                    $player->setLastName($newLastName);

                    $playerTeam = new PlayerTeam();
                    $playerTeam->setNumber($data->val($i, 2));

                    $playerTeam->setPlayer($player);
                    $playerTeam->setTeam($team);

                    $startDate= new \DateTime('2014-07-01');
                    $endDate= new \DateTime('2016-07-01');

                    $playerTeam->setStartDate($startDate);
                    $playerTeam->setEndDate($endDate);
                    $playerTeam->setStatus(PlayerTeam::PLAYER_TEAM_CURRENT);

                    $em->persist($player);
                    $em->persist($playerTeam);

                }
            }
            else{
                $output->writeln("<info>$team with $teamId not found</info>");
            }

            $i++;
        }

        $em->flush();

        $output->writeln("<info>Success!</info>");
    }
}
