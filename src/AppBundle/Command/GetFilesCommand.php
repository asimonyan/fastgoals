<?php
/**
 * Created by PhpStorm.
 * User: aram
 * Date: 7/29/15
 * Time: 6:43 PM
 */

namespace AppBundle\Command;

use AppBundle\Entity\Player;
use AppBundle\Entity\PlayerTeam;
use Doctrine\ORM\PersistentCollection;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use \PHPExcelReader\SpreadsheetReader as Reader;

/**
 * Class GetFilesCommand
 * @package AppBundle\Command
 */
class GetFilesCommand extends ContainerAwareCommand
{

    /**
     *
     */
    protected function configure()
    {
        $this
            ->setName('fastgoal:files:integrate')
            ->setDescription('integrate files from media bundle')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        // get container
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getManager();

        $posts = $em->getRepository("AppBundle:Post")->findAll();

        if($posts ){

            foreach($posts as $post){

                // get list photo
                $listPhoto = $post->getListPhoto();
                $listPath = $this->getImagePath($listPhoto, 'reference');

                if(is_file($listPath)){

                    $post->setListPhotoOriginalName($listPhoto->getName());
                    $post->setListPhotoSize($listPhoto->getSize());
                    $post->setListPhotoName($listPhoto->getProviderReference());
                    $newListPath = $post->getListAbsolutePath() . $post->getListPhotoName() ;
                    copy($listPath, $newListPath);
                }

                // get right photo
                $rightPhoto = $post->getRightPhoto();
                $rightPath = $this->getImagePath($rightPhoto, 'reference');

                if(is_file($rightPath)){

                    $post->setRightPhotoOriginalName($rightPhoto->getName());
                    $post->setRightPhotoSize($rightPhoto->getSize());
                    $post->setRightPhotoName($rightPhoto->getProviderReference());
                    $newRightPath = $post->getRightAbsolutePath() . $post->getRightPhotoName() ;
                    copy($rightPath, $newRightPath);
                }

                // get social photo
                $socialPhoto = $post->getSocialPhoto();
                $socialPath = $this->getImagePath($socialPhoto, 'reference');

                if(is_file($socialPath)){

                    $post->setSocialPhotoOriginalName($socialPhoto->getName());
                    $post->setSocialPhotoSize($socialPhoto->getSize());
                    $post->setSocialPhotoName($socialPhoto->getProviderReference());
                    $newSocialPath = $post->getSocialAbsolutePath() . $post->getSocialPhotoName() ;
                    copy($socialPath, $newSocialPath);
                }


                $em->persist($post);

            }

            $em->flush();
        }

    }


    public function getImagePath($media, $context)
    {
        $container =  $this->getContainer();

        // get web path
        $webPath = $container->get('kernel')->getRootDir() . "/../web";


        if($media)
        {
            $mediaService = $container->get('sonata.media.pool');

            $provider = $mediaService
                ->getProvider($media->getProviderName());

            $format = $provider->getFormatName($media, $context);
            $path = $provider->generatePublicUrl($media, $format);

            return $webPath . $path;
        }

        return null;
    }
}
