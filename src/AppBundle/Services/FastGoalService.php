<?php
/**
 * Created by PhpStorm.
 * User: aram
 * Date: 7/12/15
 * Time: 1:37 PM
 */

namespace AppBundle\Services;

use AppBundle\Entity\Ad;
use AppBundle\Entity\MainPost;
use AppBundle\Entity\Post;
use Symfony\Component\DependencyInjection\Container;

/**
 * Class FastGoalService
 * @package Ads\MainBundle\Services
 */
class FastGoalService
{
    // constant for params
    const DATE = 'date';
    const CATEGORY = 'league';
    const TEAM = 'team';
    const PLAYER = 'player';

    private $calendarObject = array();

    /**
     * @var \Symfony\Component\DependencyInjection\Container
     */
    protected  $container;

    /**
     * @var
     */
    protected $em;

    /**
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->em = $container->get('doctrine')->getManager();
    }


    /**
     * This function is used to get date start end end
     *
     * @param $dateSlug
     * @return array|bool
     */
    public  function getStartAndEndDates($dateSlug)
    {
        // empty start date
        $startDate = null;
        // empty end date
        $endDate = null;

        $result = false;

        // explode date object
        $currentDate = explode("-", $dateSlug);
        // get start date
        $startDate = $currentDate[0];

        // check is end date send
        if (count($currentDate) == 2) {

            // get end date
            $endDate = $currentDate[1];

        }
        else {

            // set end date to start date
            $endDate = $startDate;
        }

        // try ty get start date
        try{
            $startDate = new \DateTime($startDate);
        }
            // set to null if error
        catch (\Exception $e) {

            $startDate = null;
        }

        // try to get end date
        try{
            $endDate = new \DateTime($endDate);
        }
            // set null if error
        catch (\Exception $e) {

            $endDate = null;
        }

        // check start date and end date
        if (!is_null($startDate ) && !is_null($endDate)){

            // generate an array and return
            $result = array(
                'startDate' => $startDate->format("Y-m-d"),
                'endDate' => $endDate->format("Y-m-d"));
        }


        return $result;
    }

    //TODO: change if necessary

    /**
     * @return array
     */
    public  function getLastTwelveMonths()
    {
        $labels = array(1=>"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
        $months = array();

        $date = new \DateTime();

        $currentYear = $date->format('Y');// 2014;//(int)date("Y");
        $currentMonth = (int)$date->format('m');


        for ($i = 0; $i < 12; $i++) {
            if ($currentMonth < 1) {
                $currentMonth = 12;
                $currentYear -= 1;
            }

            $startDate = date("d.m.Y", mktime(0, 0, 0, $currentMonth, 1, $currentYear));
            $endDate = date("t.m.Y", strtotime($startDate));

            $label = $labels[$currentMonth] . " " . $currentYear;
            $slug = $startDate . "-" . $endDate;

            $months[] = array(
                "label" => $label,
                "slug" => $slug
            );

            $this->calendarObject[$slug] = $label;

            $currentMonth--;
        }
        return $months;
    }

    /**
     * @return string
     */
    public  function getLastWeekDates()
    {
        $previous_week = strtotime("-1 week +1 day");

        $start_week = strtotime("last sunday midnight", $previous_week);
        $end_week = strtotime("next saturday", $start_week);

        $start_week = date("d.m.Y", $start_week);
        $end_week = date("d.m.Y", $end_week);

        $slug = $start_week . "-" . $end_week;

        $this->calendarObject[$slug] = "Last Week";

        return $slug;
    }

    /**
     * @return string
     */
    public  function getLastMonthDates()
    {
        $start_day = date('d.m.Y', strtotime('first day of last month'));
        $end_day = date('d.m.Y', strtotime('last day of last month'));

        $slug = $start_day . "-" . $end_day;

        $this->calendarObject[$slug] = "Last Month";

        return $slug;
    }

    /**
     * @param string $dateSlug
     * @return bool|string
     */
    public  function getDateText($dateSlug)
    {
        $result = "All Time";

        if ($dateSlug != self::DATE) {
            $dates = explode("-", $dateSlug);
            if (count($dates) == 1) {
                $result = date("d M Y", strtotime($dates[0]));
            } else {
                $result = $this->calendarObject[$dateSlug];
            }
        }

        return $result;
    }

    /**
     * This function used to return image`s url ba given Sonata Media Bundle `s object
     *
     * @param $media
     * @param $context
     * @return mixed
     */
    public function getImageUrl($media, $context = 'small')
    {
        // get web path
        $webPath = $this->container->get('kernel')->getRootDir() . "/../web";

        // default image
        $defImage = '/bundles/app/images/no_photo.jpg';

        $imageUrl['src'] = $defImage ;
        $imageUrl['width'] = 267;
        $imageUrl['height'] =  0;

        if($media)
        {
            $width = 267;

            $mediaService = $this->container->get('sonata.media.pool');

            $provider = $mediaService
                ->getProvider($media->getProviderName());

            $format = $provider->getFormatName($media, $context);
            $imageUrl['src'] = $provider->generatePublicUrl($media, $format);

            $imageDir = $webPath . $imageUrl['src'] ;

            if(! is_file($imageDir)){
                $imageUrl['src'] = $defImage ;
                $imageUrl['width'] = 267;
                $imageUrl['height'] =  0;
                return $imageUrl;

            }

            $size =  getimagesize($imageDir);
            $imageUrl['width'] = 267;
            $imageUrl['height'] =  $width * $size[1] / $size[0];

            return $imageUrl;
        }

        return $imageUrl;
    }


    /**
     * @param $offset
     * @param $limit
     * @param $l
     * @param $t
     * @param $n
     * @param $p
     * @param $d
     * @param $m
     * @return array
     */
    public function getItems($offset, $limit, $l, $t, $n, $p, $d, $m)
    {
        // empty ads array
        $ads = array();

        // get entity manager
        $em = $this->container->get('doctrine')->getManager();

        // get doctrine filters
        $filters = $em->getFilters();

        // enable filter
        $filters->enable('publish_post');

        // get ads array
        $adsArray = $em->getRepository('AppBundle:Ad')->findAllOrdered();

        // loop for ads array
        foreach ($adsArray as $adArray) {

            // get images data
            $images = $adArray->getImageUrl();

            // ads array
            $ads[] = array(
                "id" => $adArray->getId(),
                "title" => $adArray->getTitle(),
                "slug" => $adArray->getSlug(),
                "position" => $adArray->getPosition() - 1,
                "url" => $adArray->getUrl(),
                "image" => $images['src'],
                "width" => $images['width'],
                "height" => $images['height'],
                "isAdGoogle" => $adArray->getType() == Ad::AD_SENSE ? true : false
            );
        }

        // empty data
        $offsetShift = 0;
        $limitShift = 0;
        $includedAds = array();

        //get position repository
        $postsRepo = $em->getRepository('AppBundle:Post');

        // find posts
        $posts = $postsRepo->findByPosts($offset - $offsetShift, $limit - $limitShift, $l, $t, $n, $p, $d, $m);

        if($posts){
            // loop for ads
            foreach ($ads as $ad) {

                // get position
                $position = $ad["position"];
                if ($position < $offset) {
                    $offsetShift++;
                }
                elseif ($position >= $offset && $position < $offset + $limit) {
                    $limitShift++;
                    $ad["isAd"] = true;
                    $includedAds[$position] = $ad;
                }
            }
        }

        // max view
        $postMaxViews = $postsRepo->getPostsMaxViews()["maxViews"];

        //posts
        $postsObject = array();

        // loop for posts
        foreach ($posts as $post) {

            // get views
            $views = $post->getViews();

            // check views
            if((int)$postMaxViews == 0){
                $viewsProgress = $views * 100 / 1;
            }
            else{
                $viewsProgress = $views * 100 / $postMaxViews;
            }

            // get image data
            $image = $post->getImageUrl(Post::LIST_IMAGE); // $this->getImageUrl($post->getListPhoto(), 'list');

            $postsObject[] = array(
                "id" => $post->getId(),
                "title" => $post->getTitle(),
                "slug" => $post->getSlug(),
                "date" => $post->getPostDate()->format('d.m.Y'),
                "time" => $post->getVideoDuration(),
                'showTitle' => $post->getShowTitle(),
                "image" => array(
                    "src" => $image['src'],
                    "width" => $image['width'],
                    "height" => $image['height']
                ),
                "league" => $post instanceof MainPost ? $post->getSeason()->getCategory()->getName() : ' ',
                "videoUrl" => $post->getVideo() ?  $post->getVideo() : '',
                "playWireUrl" => $post->getVideoPlayWire() ? $post->getVideoPlayWire() : '',
                "views" => $views,
                "viewsProgress" => $viewsProgress
            );
        }

        foreach ($includedAds as $position => $includedAd) {
            array_splice($postsObject, $position - $offset, 0, array($includedAd));
        }

        return $postsObject;
    }
}