<?php
/**
 * Created by PhpStorm.
 * User: aram
 * Date: 7/2/15
 * Time: 8:08 PM
 */

namespace AppBundle\Traits;

use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class RightPhoto
 * @package AppBundle\Traits
 */
trait RightPhoto
{
    /**
     * @Assert\File(
     *          maxSize="2000000",
     *          mimeTypes = {
     *              "image/png",
     *              "image/jpeg",
     *              "image/jpg",
     *              "image/gif",
     *          },
     *          mimeTypesMessage = "file.extension_error",
     *          maxSizeMessage = "file.size_error",
     * )
     */
    protected  $rightPhotoFile;

    /**
     * @ORM\Column(name="right_photo_original_name", type="string", length=255, nullable=true)
     */
    protected $rightPhotoOriginalName;

    /**
     * @ORM\Column(name="right_photo_name", type="string", length=255, nullable=true)
     */
    protected $rightPhotoName;

    /**
     * @var integer
     *
     * @ORM\Column(name="right_photo_size", type="integer", nullable=true)
     */
    protected $rightPhotoSize;

    /**
     * Sets file.
     *
     * @param UploadedFile $rightPhoto
     */
    public function setRightPhotoFile(UploadedFile $rightPhoto = null)
    {
        $this->rightPhotoFile = $rightPhoto;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getRightPhotoFile()
    {
        return $this->rightPhotoFile;
    }

    /**
     * Set rightPhotoOriginalName
     *
     * @param string $rightPhotoOriginalName
     * @return $this
     */
    public function setRightPhotoOriginalName($rightPhotoOriginalName)
    {
        $this->rightPhotoOriginalName = $rightPhotoOriginalName;

        return $this;
    }

    /**
     * Get rightPhotoOriginalName
     *
     * @return string
     */
    public function getRightPhotoOriginalName()
    {
        return $this->rightPhotoOriginalName;
    }

    /**
     * Set rightPhotoName
     *
     * @param string $rightPhotoName
     * @return $this
     */
    public function setRightPhotoName($rightPhotoName)
    {
        $this->rightPhotoName = $rightPhotoName;

        return $this;
    }

    /**
     * Get rightPhotoName
     *
     * @return string
     */
    public function getRightPhotoName()
    {
        return $this->rightPhotoName;
    }

    /**
     * Set rightPhotoSize
     *
     * @param integer $rightPhotoSize
     * @return $this
     */
    public function setRightPhotoSize($rightPhotoSize)
    {
        $this->rightPhotoSize = $rightPhotoSize;

        return $this;
    }

    /**
     * Get rightPhotoSize
     *
     * @return integer
     */
    public function getRightPhotoSize()
    {
        return $this->rightPhotoSize;
    }

    /**
     * @return string
     */
    protected function getRightPath()
    {
        return 'RightPhoto';
    }

    /**
     * @return string
     */
    public function getRightAbsolutePath()
    {
        return $this->getUploadRootDir() . '/' . $this->getRightPath() .'/';
    }

    /**
     * This function is used to return file web path
     *
     * @return string
     */
    public function getRightPhotoDownloadLink()
    {
        return '/' . $this->getUploadDir() . '/' . $this->getRightPath() . '/' . $this->rightPhotoName;
    }


    /**
     * This function is used to upload image
     *
     */
    public  function uploadForRightPhoto()
    {
        // the file property can be empty if the field is not required
        if (null == $this->getRightPhotoFile())
        {
            return;
        }
        // check file name
        if($this->getRightPhotoName()){
            // get file path
            $path = $this->getRightAbsolutePath() . $this->getRightPhotoName();

            // check file
            if(file_exists($path)){
                // remove file
                unlink($path);
            }
        }
        // get file originalName
        $this->rightPhotoOriginalName = $this->getRightPhotoFile()->getClientOriginalName();
        // get file
        $path_parts = pathinfo($this->getRightPhotoFile()->getClientOriginalName());

        $this->rightPhotoName = md5(microtime()) . '.' . $path_parts['extension'];
        // upload file
        $this->getRightPhotoFile()->move($this->getRightAbsolutePath(), $this->rightPhotoName);
        // set size
        $this->setRightPhotoSize($this->getRightPhotoFile()->getClientSize());
        // set file to null
        $this->rightPhotoFile  = null;
    }

    /**
     * @return null|string
     */
    public function getRightPhotoPath()
    {
        $path = null;

        // if have rightPhoto name
        if($this->getRightPhotoName()){

            // if file exist in web folder
            if(file_exists($this->getRightAbsolutePath() . $this->getRightPhotoName())){

                $path = $this->getRightPhotoDownloadLink();
            }
        }

        return $path;
    }
}