<?php
/**
 * Created by PhpStorm.
 * User: aram
 * Date: 7/2/15
 * Time: 8:08 PM
 */

namespace AppBundle\Traits;

use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class ListPhoto
 * @package AppBundle\Traits
 */
trait ListPhoto
{
    /**
     * @Assert\File(
     *          maxSize="2000000",
     *          mimeTypes = {
     *              "image/png",
     *              "image/jpeg",
     *              "image/jpg",
     *              "image/gif",
     *          },
     *          mimeTypesMessage = "file.extension_error",
     *          maxSizeMessage = "file.size_error",
     * )
     */
    protected  $listPhotoFile;

    /**
     * @ORM\Column(name="list_photo_original_name", type="string", length=255, nullable=true)
     */
    protected $listPhotoOriginalName;

    /**
     * @ORM\Column(name="list_photo_name", type="string", length=255, nullable=true)
     */
    protected $listPhotoName;

    /**
     * @var integer
     *
     * @ORM\Column(name="list_photo_size", type="integer", nullable=true)
     */
    protected $listPhotoSize;

    /**
     * Sets file.
     *
     * @param UploadedFile $listPhoto
     */
    public function setListPhotoFile(UploadedFile $listPhoto = null)
    {
        $this->listPhotoFile = $listPhoto;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getListPhotoFile()
    {
        return $this->listPhotoFile;
    }

    /**
     * Set listPhotoOriginalName
     *
     * @param string $listPhotoOriginalName
     * @return $this
     */
    public function setListPhotoOriginalName($listPhotoOriginalName)
    {
        $this->listPhotoOriginalName = $listPhotoOriginalName;

        return $this;
    }

    /**
     * Get listPhotoOriginalName
     *
     * @return string
     */
    public function getListPhotoOriginalName()
    {
        return $this->listPhotoOriginalName;
    }

    /**
     * Set listPhotoName
     *
     * @param string $listPhotoName
     * @return $this
     */
    public function setListPhotoName($listPhotoName)
    {
        $this->listPhotoName = $listPhotoName;

        return $this;
    }

    /**
     * Get listPhotoName
     *
     * @return string
     */
    public function getListPhotoName()
    {
        return $this->listPhotoName;
    }

    /**
     * Set listPhotoSize
     *
     * @param integer $listPhotoSize
     * @return $this
     */
    public function setListPhotoSize($listPhotoSize)
    {
        $this->listPhotoSize = $listPhotoSize;

        return $this;
    }

    /**
     * Get listPhotoSize
     *
     * @return integer
     */
    public function getListPhotoSize()
    {
        return $this->listPhotoSize;
    }

    /**
     * @return string
     */
    protected function getListPath()
    {
        return 'ListPhoto';
    }

    /**
     * @return string
     */
    public function getListAbsolutePath()
    {
        return $this->getUploadRootDir() . '/' . $this->getListPath() .'/';
    }

    /**
     * This function is used to return file web path
     *
     * @return string
     */
    public function getListPhotoDownloadLink()
    {
        return '/' . $this->getUploadDir() . '/' . $this->getListPath() . '/' . $this->listPhotoName;
    }


    /**
     * This function is used to upload image
     *
     */
    public  function uploadForListPhoto()
    {
        // the file property can be empty if the field is not required
        if (null == $this->getListPhotoFile())
        {
            return;
        }
        // check file name
        if($this->getListPhotoName()){
            // get file path
            $path = $this->getListAbsolutePath() . $this->getListPhotoName();

            // check file
            if(file_exists($path)){
                // remove file
                unlink($path);
            }
        }
        // get file originalName
        $this->listPhotoOriginalName = $this->getListPhotoFile()->getClientOriginalName();
        // get file
        $path_parts = pathinfo($this->getListPhotoFile()->getClientOriginalName());

        $this->listPhotoName = md5(microtime()) . '.' . $path_parts['extension'];
        // upload file
        $this->getListPhotoFile()->move($this->getListAbsolutePath(), $this->listPhotoName);
        // set size
        $this->setListPhotoSize($this->getListPhotoFile()->getClientSize());
        // set file to null
        $this->listPhotoFile  = null;
    }

    /**
     * @return null|string
     */
    public function getListPhotoPath()
    {
        $path = null;

        // if have listPhoto name
        if($this->getListPhotoName()){

            // if file exist in web folder
            if(file_exists($this->getListAbsolutePath() . $this->getListPhotoName())){

                $path = $this->getListPhotoDownloadLink();
            }
        }

        return $path;
    }
}