<?php
/**
 * Created by PhpStorm.
 * User: aram
 * Date: 7/2/15
 * Time: 8:08 PM
 */

namespace AppBundle\Traits;

use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class SocialPhoto
 * @package AppBundle\Traits
 */
trait SocialPhoto
{
    /**
     * @Assert\File(
     *          maxSize="2000000",
     *          mimeTypes = {
     *              "image/png",
     *              "image/jpeg",
     *              "image/jpg",
     *              "image/gif",
     *          },
     *          mimeTypesMessage = "file.extension_error",
     *          maxSizeMessage = "file.size_error",
     * )
     */
    protected  $socialPhotoFile;

    /**
     * @ORM\Column(name="social_photo_original_name", type="string", length=255, nullable=true)
     */
    protected $socialPhotoOriginalName;

    /**
     * @ORM\Column(name="social_photo_name", type="string", length=255, nullable=true)
     */
    protected $socialPhotoName;

    /**
     * @var integer
     *
     * @ORM\Column(name="social_photo_size", type="integer", nullable=true)
     */
    protected $socialPhotoSize;

    /**
     * Sets file.
     *
     * @param UploadedFile $socialPhoto
     */
    public function setSocialPhotoFile(UploadedFile $socialPhoto = null)
    {
        $this->socialPhotoFile = $socialPhoto;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getSocialPhotoFile()
    {
        return $this->socialPhotoFile;
    }

    /**
     * Set socialPhotoOriginalName
     *
     * @param string $socialPhotoOriginalName
     * @return $this
     */
    public function setSocialPhotoOriginalName($socialPhotoOriginalName)
    {
        $this->socialPhotoOriginalName = $socialPhotoOriginalName;

        return $this;
    }

    /**
     * Get socialPhotoOriginalName
     *
     * @return string
     */
    public function getSocialPhotoOriginalName()
    {
        return $this->socialPhotoOriginalName;
    }

    /**
     * Set socialPhotoName
     *
     * @param string $socialPhotoName
     * @return $this
     */
    public function setSocialPhotoName($socialPhotoName)
    {
        $this->socialPhotoName = $socialPhotoName;

        return $this;
    }

    /**
     * Get socialPhotoName
     *
     * @return string
     */
    public function getSocialPhotoName()
    {
        return $this->socialPhotoName;
    }

    /**
     * Set socialPhotoSize
     *
     * @param integer $socialPhotoSize
     * @return $this
     */
    public function setSocialPhotoSize($socialPhotoSize)
    {
        $this->socialPhotoSize = $socialPhotoSize;

        return $this;
    }

    /**
     * Get socialPhotoSize
     *
     * @return integer
     */
    public function getSocialPhotoSize()
    {
        return $this->socialPhotoSize;
    }

    /**
     * @return string
     */
    protected function getSocialPath()
    {
        return 'SocialPhoto';
    }

    /**
     * @return string
     */
    public function getSocialAbsolutePath()
    {
        return $this->getUploadRootDir() . '/' . $this->getSocialPath() .'/';
    }

    /**
     * This function is used to return file web path
     *
     * @return string
     */
    public function getSocialPhotoDownloadLink()
    {
        return '/' . $this->getUploadDir() . '/' . $this->getSocialPath() . '/' . $this->socialPhotoName;
    }


    /**
     * This function is used to upload image
     *
     */
    public  function uploadForSocialPhoto()
    {
        // the file property can be empty if the field is not required
        if (null == $this->getSocialPhotoFile())
        {
            return;
        }
        // check file name
        if($this->getSocialPhotoName()){
            // get file path
            $path = $this->getSocialAbsolutePath() . $this->getSocialPhotoName();

            // check file
            if(file_exists($path)){
                // remove file
                unlink($path);
            }
        }
        // get file originalName
        $this->socialPhotoOriginalName = $this->getSocialPhotoFile()->getClientOriginalName();
        // get file
        $path_parts = pathinfo($this->getSocialPhotoFile()->getClientOriginalName());

        $this->socialPhotoName = md5(microtime()) . '.' . $path_parts['extension'];
        // upload file
        $this->getSocialPhotoFile()->move($this->getSocialAbsolutePath(), $this->socialPhotoName);
        // set size
        $this->setSocialPhotoSize($this->getSocialPhotoFile()->getClientSize());
        // set file to null
        $this->socialPhoto  = null;
    }

    /**
     * @return null|string
     */
    public function getSocialPhotoPath()
    {
        $path = null;

        // if have socialPhoto name
        if($this->getSocialPhotoName()){

            // if file exist in web folder
            if(file_exists($this->getSocialAbsolutePath() . $this->getSocialPhotoName())){

                $path = $this->getSocialPhotoDownloadLink();
            }
        }

        return $path;
    }
}