<?php
/**
 * Created by PhpStorm.
 * User: aram
 * Date: 7/2/15
 * Time: 8:08 PM
 */

namespace AppBundle\Traits;

use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class FileInfo
 * @package Lp\UserBundle\Traits
 */
trait FileInfo
{
    /**
     * This function is used to return file web path
     *
     * @return string
     */
    public function getUploadRootDir()
    {
        return __DIR__. '/../../../web/' . $this->getUploadDir();
    }

    /**
     * Upload folder name
     *
     * @return string
     */
    protected function getUploadDir()
    {
        return 'uploads';
    }
}