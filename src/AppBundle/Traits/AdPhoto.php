<?php
/**
 * Created by PhpStorm.
 * User: aram
 * Date: 7/2/15
 * Time: 8:08 PM
 */

namespace AppBundle\Traits;

use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class AdPhoto
 * @package AppBundle\Traits
 */
trait AdPhoto
{
    /**
     * @Assert\File(
     *          maxSize="2000000",
     *          mimeTypes = {
     *              "image/png",
     *              "image/jpeg",
     *              "image/jpg",
     *              "image/gif",
     *          },
     *          mimeTypesMessage = "file.extension_error",
     *          maxSizeMessage = "file.size_error",
     * )
     */
    protected  $adPhoto;

    /**
     * @ORM\Column(name="ad_photo_original_name", type="string", length=255, nullable=true)
     */
    protected $adPhotoOriginalName;

    /**
     * @ORM\Column(name="ad_photo_name", type="string", length=255, nullable=true)
     */
    protected $adPhotoName;

    /**
     * @var integer
     *
     * @ORM\Column(name="ad_photo_size", type="integer", nullable=true)
     */
    protected $adPhotoSize;

    /**
     * Sets file.
     *
     * @param UploadedFile $adPhoto
     */
    public function setAdPhoto(UploadedFile $adPhoto = null)
    {
        $this->adPhoto = $adPhoto;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getAdPhoto()
    {
        return $this->adPhoto;
    }

    /**
     * Set adPhotoOriginalName
     *
     * @param string $adPhotoOriginalName
     * @return $this
     */
    public function setAdPhotoOriginalName($adPhotoOriginalName)
    {
        $this->adPhotoOriginalName = $adPhotoOriginalName;

        return $this;
    }

    /**
     * Get adPhotoOriginalName
     *
     * @return string
     */
    public function getAdPhotoOriginalName()
    {
        return $this->adPhotoOriginalName;
    }

    /**
     * Set adPhotoName
     *
     * @param string $adPhotoName
     * @return $this
     */
    public function setAdPhotoName($adPhotoName)
    {
        $this->adPhotoName = $adPhotoName;

        return $this;
    }

    /**
     * Get adPhotoName
     *
     * @return string
     */
    public function getAdPhotoName()
    {
        return $this->adPhotoName;
    }

    /**
     * Set adPhotoSize
     *
     * @param integer $adPhotoSize
     * @return $this
     */
    public function setAdPhotoSize($adPhotoSize)
    {
        $this->adPhotoSize = $adPhotoSize;

        return $this;
    }

    /**
     * Get adPhotoSize
     *
     * @return integer
     */
    public function getAdPhotoSize()
    {
        return $this->adPhotoSize;
    }

    /**
     * @return string
     */
    protected function getPath()
    {
        return 'AdPhoto';
    }

    /**
     * This function is used to return file web path
     *
     * @return string
     */
    public function getAdPhotoDownloadLink()
    {
        return '/' . $this->getUploadDir() . '/' . $this->getPath() . '/' . $this->adPhotoName;
    }


    /**
     * This function is used to upload image
     *
     */
    public  function uploadForAdPhoto()
    {
        // the file property can be empty if the field is not required
        if (null == $this->getAdPhoto())
        {
            return;
        }
        // check file name
        if($this->getAdPhotoName()){
            // get file path
            $path = $this->getAbsolutePath() . $this->getAdPhotoName();

            // check file
            if(file_exists($path)){
                // remove file
                unlink($path);
            }
        }
        // get file originalName
        $this->adPhotoOriginalName = $this->getAdPhoto()->getClientOriginalName();
        // get file
        $path_parts = pathinfo($this->getAdPhoto()->getClientOriginalName());

        $this->adPhotoName = md5(microtime()) . '.' . $path_parts['extension'];
        // upload file
        $this->getAdPhoto()->move($this->getAbsolutePath(), $this->adPhotoName);
        // set size
        $this->setAdPhotoSize($this->getAdPhoto()->getClientSize());
        // set file to null
        $this->adPhoto  = null;
    }

    /**
     * @return null|string
     */
    public function getAdPhotoPath()
    {
        $path = null;

        // if have adPhoto name
        if($this->getAdPhotoName()){

            // if file exist in web folder
            if(file_exists($this->getAbsolutePath() . $this->getAdPhotoName())){

                $path = $this->getAdPhotoDownloadLink();
            }
        }

        return $path;
    }

    /**
     * @return string
     */
    public function getAbsolutePath()
    {
        return $this->getUploadRootDir() . '/' . $this->getPath() .'/';
    }
}