<?php

namespace AppBundle\Admin;

use AppBundle\Entity\Ad;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class AdsAdmin extends Admin
{

    protected  $baseRouteName = 'ad';
    protected  $baseRoutePattern = 'ad';

    /**
     * Row show configuration
     *
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     * @return void
     */
    protected function configureShowField(ShowMapper $showMapper)
    {
        $showMapper
                ->add('position', null, array('label' => 'Position'))
                ->add('url', null, array('label' => 'Url'));
    }

    /**
     * List show configuration
     *
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->addIdentifier('title', null, array('label' => 'Title'))
                ->addIdentifier('position', null, array('label' => 'Position'))
                ->addIdentifier('url', null, array('label' => 'Url'))
                ->addIdentifier('width', null, array('label' => 'Width'))
                ->addIdentifier('height', null, array('label' => 'Height'))
                ->add('_action', 'actions', array('actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array()
                )));
    }

    /**
     * Row form edit configuration
     *
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $types = array(
            Ad::AD => "Ad",
            Ad::AD_SENSE => "AdSense",
            Ad::RIGHT => "Right Part"
        );

        $formMapper
                ->with('General')
                ->add('type', 'choice', array(
                'choices' => $types,
                'empty_value' => "Choose Type"
            ))
                ->add('title', null, array('required' => true))
                ->add('position', null, array('required' => true))
                ->add('url', null, array('required' => false))
                ->add('width', null, array('required' => true))
                ->add('height', null, array('required' => false))
                ->add('slug', null, array('required' => false))
                ->add('adPhoto', 'file', array('required' => false))
            ->end();
    }

    /**
     * Fields in list rows search
     *
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('position')
                ->add('slug');
    }

    /**
     * {@inheritdoc}
     */
    public function preUpdate($ad)
    {
        $ad->uploadForAdPhoto();
    }
}