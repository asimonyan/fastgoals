<?php
/**
 * Created by PhpStorm.
 * User: aram
 * Date: 7/5/15
 * Time: 1:43 PM
 */

namespace AppBundle\Admin;

use AppBundle\Entity\PlayerTeam;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class PlayerTeamAdmin extends Admin
{

    protected  $baseRouteName = 'player-team';
    protected  $baseRoutePattern = 'player-team';


    /**
     * Row form edit configuration
     *
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('number', null, array('required' => true))
            ->add('team', null, array('required' => true))
            ->add('startDate', 'date', array('widget' => 'choice', 'required' => false, 'years' => range(1990, 2030)))
            ->add('endDate', 'date', array('widget' => 'choice', 'required' => false, 'years' => range(1990, 2030)))
            ->add('status', 'choice', array(
                'choices' => array(
                    PlayerTeam::PLAYER_TEAM_OLD => 'Old',
                    PlayerTeam::PLAYER_TEAM_CURRENT => 'Current'),
                'required' => true,
            ))
            ->end();
    }

}