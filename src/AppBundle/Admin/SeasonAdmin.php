<?php
/**
 * Created by PhpStorm.
 * User: aram
 * Date: 7/5/15
 * Time: 1:10 PM
 */

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class SeasonAdmin extends Admin
{

    protected  $baseRouteName = 'season';
    protected  $baseRoutePattern = 'season';

    /**
     * Row show configuration
     *
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     * @return void
     */
    protected function configureShowField(ShowMapper $showMapper)
    {
        $showMapper
            ->add('name', null, array('label' => 'Name'))
            ->add('startDate', null, array('label' => 'Start Date'))
            ->add('endDate', null, array('label' => 'End Date'));
    }

    /**
     * List show configuration
     *
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('category', null, array('label' => 'Name'))
            ->addIdentifier('name', null, array('label' => 'Name'))
            ->addIdentifier('startDate', null, array('label' => 'Start Date'))
            ->addIdentifier('endDate', null, array('label' => 'End Date'))
            ->add('_action', 'actions', array('actions' => array(
                'show' => array(),
                'edit' => array(),
                'delete' => array()
            )));
    }

    /**
     * Row form edit configuration
     *
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('category', null, array('required' => true))
            ->add('name', null, array('required' => true))
            ->add('startDate', 'date', array('widget' => 'choice', 'required' => false, 'years' => range(2005, 2020)))
            ->add('endDate', 'date', array('widget' => 'choice', 'required' => false, 'years' => range(2005, 2020)))
            ->add('slug', null, array('required' => false))
        ;
    }

    /**
     * Fields in list rows search
     *
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('slug');
    }


}