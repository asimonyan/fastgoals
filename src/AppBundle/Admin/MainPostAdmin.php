<?php
/**
 * Created by PhpStorm.
 * User: aram
 * Date: 7/5/15
 * Time: 1:15 PM
 */

namespace AppBundle\Admin;

use AppBundle\Entity\Event;
use AppBundle\Form\Statistic;
use AppBundle\Form\Type\StatisticType;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class MainPostAdmin extends Admin
{

    protected  $baseRouteName = 'main-post';
    protected  $baseRoutePattern = 'main-post';

    /**
     * Row show configuration
     *
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     * @return void
     */
    protected function configureShowField(ShowMapper $showMapper)
    {
        $showMapper
            ->add('title', null, array('label' => 'Title'))
            ->add('video', null, array('label' => 'Video url'))
            ->add('videoPlayWire', null, array('label' => 'video PlayWire url'));

    }

    /**
     * List show configuration
     *
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('title', null, array('label' => 'Title'))
            ->add('video', null, array('label' => 'Video url'))
            ->add('videoPlayWire', null, array('label' => 'Video PlayWire url', 'template' => 'AppBundle:Admin:list_wire.html.twig'))
            ->add('season', null, array('label' => 'Categories Dates'))
            ->add('slug', null, array('label' => 'Slug'))
            ->add('position')
            ->add('publish', 'boolean', array('label' => 'Publish'))
            ->add('_action', 'actions', array('actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array()
                )));

    }

    /**
     * Row form edit configuration
     *
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $postID = $teamHomeID = $teamGuestID = 0;
        if ($this->id($this->getSubject())) {
            // EDIT
            $postID = $this->getSubject()->getId();
            $postRepository = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager()->getRepository('AppBundle:MainPost');
            $post = $postRepository->findOneById($postID);
            $teamHomeID = $post->getTeamHome()->getId();
            $teamGuestID = $post->getTeamGuest()->getId();
        }

        $formMapper
            ->add('postDate', 'date', array('widget' => 'choice', 'required' => false,'years' => range(1980, 2030)))
            ->add('season', null, array('attr' => array('class' => 'clearfix')))
            ->add('title', null, array('label' => 'Title'))
            ->add('slug', null, array('required' => false))
            ->add('video', null, array('label' => 'Video url', 'required' => false))
            ->add('videoDuration', null, array('label' => 'Video duration'))
            ->add('videoPlayWire', null, array('label' => ' PlayWire url','required' => false))
            ->add('score', null, array('label' => 'Score'))
            ->add('statistic', 'sonata_type_immutable_array', array(
                'label'=>'Statistic',
                'required' => false,
                'keys' => array(
                    array('statistic', new Statistic(), array('label'=>' ', 'required' => false)),
                )
            ))

            ->add('teamHome', null, array('required' => true,
                    'empty_value'=>'Select Home Team',
                    'label' => 'Home team',
                    'attr' => array('class' => 'clearfix',
                        'ng-model' => 'teamHome'))
            )

            ->add('event_type_home', 'players', array(
                'data' => $postID,
                'mapped' => false,
                'label' => ' ',
                'required' => false,
                'attr' => array('teamType' => 0,
                    'teamID' => $teamHomeID,)))
            ->add('teamGuest', null, array('required' => true,
                    'empty_value'=>'Select Home Team',
                    'label' => 'Guest team',
                    'attr' => array('class' => 'clearfix',
                        'ng-model' => 'teamGuest'),
            ))
            ->add('event_type_guest', 'players', array('data' => $postID,
                'label' => ' ',
                'mapped' => false,
                'required' => false,
                'attr' => array('teamType' => 1,
                    'teamID' => $teamGuestID,)))
            ->add('listPhotoFile', 'file', array('required' => false))
            ->add('rightPhotoFile', 'file', array('required' => false))
            ->add('socialPhotoFile', 'file', array('required' => false))

            ->add('meta_kw', null, array('label' => 'Meta keywords'))
            ->add('meta_desc', null, array('label' => 'Meta description'))
            ->add('position', null, array('required' => false))
            ->add('publish', null, array('required' => false))
            ->end();
    }

    /**
     * Fields in list rows search
     *
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title', null, array('label' => 'Title'))
            ->add('season', null, array('label' => 'Categories Dates'))
            ->add('video');
    }

    /**
     * @return array
     */
    public function getFormTheme()
    {
        return array_merge(
            parent::getFormTheme(),
            array('AppBundle:Admin:players_edit.html.twig', 'AppBundle:Admin:statistic_type.html.twig')
        );

    }

    /**
     * {@inheritdoc}
     */
    public function prePersist($post)
    {
        // get container
        $container = $this->getConfigurationPool()->getContainer();

        // get entity manager
        $em = $container->get('doctrine')->getManager();

        $events = $container->get('request')->get('events');

        if(sizeof($events) == 2)
        {
            $IDs = array_keys($events);
            if(sizeof($IDs) != 2)
                return false;

            $teamHomeID = $IDs[0]; // home team id
            $teamGuestID = $IDs[1];  // guest team id

            // get repositories
            $teamRepository = $em->getRepository('AppBundle:Team');
            $playerRepository = $em->getRepository('AppBundle:Player');

            //get player IDs
            $homeTeamPlayerIDs = array_keys($events[$teamHomeID]);
            $guestTeamPlayerIDs = array_keys($events[$teamGuestID]);
            $playerIDs = array_merge($homeTeamPlayerIDs, $guestTeamPlayerIDs);

            $teams = $teamRepository->findByIDs($IDs);
            $players = $playerRepository->findByIDs($playerIDs);

            // loop for events
            foreach($events as $teamID => $playersEvent) {

                // loop for one event
                foreach($playersEvent as $playerID => $event) {

                    // lineUps
                    if(isset($event['lineUps']) && (int)$event['lineUps'] === 1){

                        //create new event, and set data
                        $eventRecord = new Event();
                        $eventRecord->setTeam($teams[$teamID]);
                        $eventRecord->setPost($post);
                        $eventRecord->setPlayer($players[$playerID]);
                        $eventRecord->setType(Event::EVENT_TYPE_LINE_UPS);
                        $em->persist($eventRecord);
                    }

                    // goals
                    if(isset($event['goal']) && (strlen($event['goal']) > 0)){
                        $playerGoals = explode(',', $event['goal']);
                        foreach($playerGoals as $value){
                            // for own goal
                            $goals = explode('-', $value);

                            if(!empty($goals) && sizeof($goals) == 2) {

                                $value = $goals[0];
                            }
                            if((int)$value > 0) {

                                $eventRecord = new Event();
                                $eventRecord->setTeam($teams[$teamID]);
                                $eventRecord->setPost($post);
                                $eventRecord->setPlayer($players[$playerID]);
                                $eventRecord->setType(Event::EVENT_TYPE_GOAL);
                                $eventRecord->setMinute($value);

                                if(isset($goals[1]) && strtolower($goals[1]) == 'o') {

                                    $eventRecord->setOwn(Event::EVENT_OWN_GOAL);
                                }
                                else {

                                    $eventRecord->setOwn(Event::EVENT_GOAL);
                                }
                                $em->persist($eventRecord);
                            }
                        }
                    }

                    // yellow Card
                    if(isset($event['yCard']) && (strlen($event['yCard']) > 0)){

                        $yCards = explode(',', $event['yCard']);

                        foreach($yCards as $value){

                            if((int)$value > 0) {

                                $eventRecord = new Event();
                                $eventRecord->setTeam($teams[$teamID]);
                                $eventRecord->setPost($post);
                                $eventRecord->setPlayer($players[$playerID]);
                                $eventRecord->setType(Event::EVENT_TYPE_YELLOW_CARD);
                                $eventRecord->setMinute($value);
                                $em->persist($eventRecord);
                            }
                        }
                    }

                    // red Card
                    if(isset($event['rCard']) && (strlen($event['rCard']) > 0)){

                        $rCards = explode(',', $event['rCard']);

                        foreach($rCards as $value){

                            if((int)$value > 0) {

                                $eventRecord = new Event();
                                $eventRecord->setTeam($teams[$teamID]);
                                $eventRecord->setPost($post);
                                $eventRecord->setPlayer($players[$playerID]);
                                $eventRecord->setType(Event::EVENT_TYPE_RED_CARD);
                                $eventRecord->setMinute($value);
                                $em->persist($eventRecord);
                            }
                        }
                    }

                    // substitute
                    if(isset($event['substitude']) && (strlen($event['substitude']) > 0)){

                        $substitute = explode(',', $event['substitude']);

                        foreach($substitute as $value){
                            if((int)$value > 0)
                            {
                                if(isset($event['lineUps'])) {

                                    if($value == reset($substitute)){
                                        $eventRecord = new Event();
                                        $eventRecord->setTeam($teams[$teamID]);
                                        $eventRecord->setPost($post);
                                        $eventRecord->setPlayer($players[$playerID]);
                                        $eventRecord->setType(Event::EVENT_TYPE_SUBSTITUDE_OUT);
                                        $eventRecord->setMinute($value);
                                        $em->persist($eventRecord);
                                    }

                                }
                                else {

                                    if($value == reset($substitute)){
                                        $eventRecord = new Event();
                                        $eventRecord->setTeam($teams[$teamID]);
                                        $eventRecord->setPost($post);
                                        $eventRecord->setPlayer($players[$playerID]);
                                        $eventRecord->setType(Event::EVENT_TYPE_SUBSTITUDE_INGOING);
                                        $eventRecord->setMinute($value);
                                        $em->persist($eventRecord);
                                    }
                                    else{
                                        $eventRecord = new Event();
                                        $eventRecord->setTeam($teams[$teamID]);
                                        $eventRecord->setPost($post);
                                        $eventRecord->setPlayer($players[$playerID]);
                                        $eventRecord->setType(Event::EVENT_TYPE_SUBSTITUDE_OUT);
                                        $eventRecord->setMinute($value);
                                        $em->persist($eventRecord);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $em->flush();
        }

        $post->uploadForListPhoto();
        $post->uploadForRightPhoto();
        $post->uploadForSocialPhoto();
    }


    /**
     * {@inheritdoc}
     */
    public function preUpdate($post)
    {

        // get container
        $container = $this->getConfigurationPool()->getContainer();

        // get doctrine manager
        $em = $container->get('doctrine')->getManager();

        // get events
        $events = $container->get('request')->get('events');

        if(sizeof($events) == 2)
        {
            $IDs = array_keys($events);
            if(sizeof($IDs) != 2)
                return false;

            $teamHomeID = $IDs[0]; // home team id
            $teamGuestID = $IDs[1];  // guest team id

            // get repositories
            $teamRepository = $em->getRepository('AppBundle:Team');
            $playerRepository = $em->getRepository('AppBundle:Player');
            $eventRepository = $em->getRepository('AppBundle:Event');

            //get player IDs
            $homeTeamPlayerIDs = array_keys($events[$teamHomeID]);
            $guestTeamPlayerIDs = array_keys($events[$teamGuestID]);
            $playerIDs = array_merge($homeTeamPlayerIDs, $guestTeamPlayerIDs);

            // teams and players, key is ID
            $teams = $teamRepository->findByIDs($IDs);
            $players = $playerRepository->findByIDs($playerIDs);

            // get all events for this post
            $allEvents = $eventRepository->findByPost($post);
            $existLineUps = $existRedCards = $existYellowCards = $existGoals = $substitudeOut = $substitudeIn = array();

            // loop for events
            foreach($allEvents as $value) {

                $playerID = $value->getPlayer()->getId();

                // switch for event
                switch($value->getType())
                {
                    case Event::EVENT_TYPE_LINE_UPS:
                        $existLineUps[$playerID] = $value;
                        break;

                    case Event::EVENT_TYPE_GOAL:
                        $existGoals[$playerID][$value->getMinute()] = $value;
                        break;

                    case Event::EVENT_TYPE_RED_CARD:
                        $existRedCards[$playerID][$value->getMinute()] = $value;
                        break;

                    case Event::EVENT_TYPE_YELLOW_CARD:
                        $existYellowCards[$playerID][$value->getMinute()] = $value;
                        break;

                    case Event::EVENT_TYPE_SUBSTITUDE_OUT:
                        $substitudeOut[$playerID][$value->getMinute()] = $value;
                        break;

                    case Event::EVENT_TYPE_SUBSTITUDE_INGOING:
                        $substitudeIn[$playerID][$value->getMinute()] = $value;
                        break;
                }
            }

            // set events
            foreach($events as $teamID => $eventsArray)
            {
                foreach($eventsArray as $playerID => $event)
                {
                    // lineUps
                    if(isset($event['lineUps']) && (int)$event['lineUps'] === 1){

                        // create if not exist
                        if(!isset($existLineUps[$playerID])) {

                            $eventRecord = new Event();
                            $eventRecord->setTeam($teams[$teamID]);
                            $eventRecord->setPost($post);
                            $eventRecord->setPlayer($players[$playerID]);
                            $eventRecord->setType(Event::EVENT_TYPE_LINE_UPS);
                            $em->persist($eventRecord);

                            if(isset($substitudeIn[$playerID])){
                                foreach($substitudeIn[$playerID] as $inEvent){

                                    if($inEvent == reset($substitudeOut[$playerID])){

                                        $inEvent->setType(Event::EVENT_TYPE_SUBSTITUDE_OUT);
                                        $em->persist($inEvent);
                                    }
                                    else{
                                        $em->remove($inEvent);
                                    }
                                }
                            }
                        }
                    }
                    elseif(isset($existLineUps[$playerID])) {

                        if(isset($substitudeOut[$playerID])){
                            $outEvent = reset($substitudeOut[$playerID]);
                            $outEvent->setType(Event::EVENT_TYPE_SUBSTITUDE_INGOING);
                            $em->persist($outEvent);
                        }

                        $em->remove($existLineUps[$playerID]);
                    }

                    // goals
                    if(isset($event['goal']) && (strlen($event['goal']) > 0)){
                        $playerGoals = explode(',', $event['goal']);

                        foreach($playerGoals as $value){

                            // for own goal
                            $goals = explode('-', $value);

                            if(!empty($goals) && sizeof($goals) == 2) {
                                $value = $goals[0];
                            }

                            // remove all
                            if(isset($existGoals[$playerID])) {

                                foreach($existGoals[$playerID] as $minute => $goal){
                                    if(!in_array($minute, $playerGoals)){
                                        $em->remove($goal);
                                    }
                                }
                            }

                            // create if not exist
                            if(!isset($existGoals[$playerID][(int)$value]) && (int)$value > 0) {

                                $eventRecord = new Event();
                                $eventRecord->setTeam($teams[$teamID]);
                                $eventRecord->setPost($post);
                                $eventRecord->setPlayer($players[$playerID]);
                                $eventRecord->setType(Event::EVENT_TYPE_GOAL);
                                $eventRecord->setMinute($value);

                                if(isset($goals[1]) && strtolower($goals[1]) == 'o') {

                                    $eventRecord->setOwn(Event::EVENT_OWN_GOAL);
                                }
                                else {

                                    $eventRecord->setOwn(Event::EVENT_GOAL);
                                }
                                $em->persist($eventRecord);
                            }

                        }
                    }
                    elseif(!empty($existGoals[$playerID])){

                        // remove goal if not exist
                        foreach($existGoals[$playerID] as $goal){
                            $em->remove($goal);
                        }
                    }

                    // yellow Card
                    if(isset($event['yCard']) && (strlen($event['yCard']) > 0)){

                        $yCards = explode(',', $event['yCard']);

                        foreach($yCards as $value){
                            // remove all
                            if(isset($existYellowCards[$playerID]))
                            {
                                foreach($existYellowCards[$playerID] as $minute => $yCard){
                                    if(!in_array($minute, $yCards)){
                                        $em->remove($yCard);
                                    }
                                }
                            }
                            // create if not exist
                            if(!isset($existYellowCards[$playerID][(int)$value]) && (int)$value > 0)
                            {
                                $eventRecord = new Event();
                                $eventRecord->setTeam($teams[$teamID]);
                                $eventRecord->setPost($post);
                                $eventRecord->setPlayer($players[$playerID]);
                                $eventRecord->setType(Event::EVENT_TYPE_YELLOW_CARD);
                                $eventRecord->setMinute($value);
                                $em->persist($eventRecord);
                            }
                        }
                    }
                    elseif(!empty($existYellowCards[$playerID])){

                        // remove ycards if not exist
                        foreach($existYellowCards[$playerID] as $yCard){
                            $em->remove($yCard);
                        }
                    }

                    // red Card
                    if(isset($event['rCard']) && (strlen($event['rCard']) > 0)){

                        $rCards = explode(',', $event['rCard']);

                        foreach($rCards as $value){
                            // remove all
                            if(isset($existRedCards[$playerID])) {

                                foreach($existRedCards[$playerID] as $minute => $rCard){
                                    if(!in_array($minute, $rCards)){
                                        $em->remove($rCard);
                                    }
                                }
                            }
                            // create if not exist
                            if(!isset($existRedCards[$playerID][(int)$value]) && (int)$value > 0)
                            {
                                $eventRecord = new Event();
                                $eventRecord->setTeam($teams[$teamID]);
                                $eventRecord->setPost($post);
                                $eventRecord->setPlayer($players[$playerID]);
                                $eventRecord->setType(Event::EVENT_TYPE_RED_CARD);
                                $eventRecord->setMinute($value);
                                $em->persist($eventRecord);
                            }
                        }
                    }
                    elseif(!empty($existRedCards[$playerID])){
                        // remove red cards if not exist
                        foreach($existRedCards[$playerID] as $rCard){
                            $em->remove($rCard);
                        }
                    }

                    // substitude
                    if(isset($event['substitude']) && (strlen($event['substitude']) > 0)){

                        $substitude = explode(',', $event['substitude']);

                        foreach($substitude as $value){
                            // create if not exist
                            if(!isset($substitudeIn[$playerID][(int)$value]) && !isset($substitudeOut[$playerID][(int)$value]) && (int)$value > 0) {

                                if(isset($event['lineUps'])) {

                                    if($value == reset($substitude)){
                                        $eventRecord = new Event();
                                        $eventRecord->setTeam($teams[$teamID]);
                                        $eventRecord->setPost($post);
                                        $eventRecord->setPlayer($players[$playerID]);
                                        $eventRecord->setType(Event::EVENT_TYPE_SUBSTITUDE_OUT);
                                        $eventRecord->setMinute($value);
                                        $em->persist($eventRecord);

                                        $this->removeSubstitude($substitudeOut, $substitudeIn, $playerID, $em);
                                    }
                                }
                                else {

                                    if($value == reset($substitude)){
                                        $eventRecord = new Event();
                                        $eventRecord->setTeam($teams[$teamID]);
                                        $eventRecord->setPost($post);
                                        $eventRecord->setPlayer($players[$playerID]);
                                        $eventRecord->setType(Event::EVENT_TYPE_SUBSTITUDE_INGOING);
                                        $eventRecord->setMinute($value);
                                        $em->persist($eventRecord);

                                        $this->removeSubstitude($substitudeOut, $substitudeIn, $playerID, $em);
                                    }
                                    else{
                                        $eventRecord = new Event();
                                        $eventRecord->setTeam($teams[$teamID]);
                                        $eventRecord->setPost($post);
                                        $eventRecord->setPlayer($players[$playerID]);
                                        $eventRecord->setType(Event::EVENT_TYPE_SUBSTITUDE_OUT);
                                        $eventRecord->setMinute($value);
                                        $em->persist($eventRecord);

                                        $this->removeSubstitude($substitudeOut, $substitudeIn, $playerID, $em);
                                    }
                                }
                            }
                        }
                    }
                    else{
                        $this->removeSubstitude($substitudeOut, $substitudeIn, $playerID, $em);
                    }
                }
            }
            $em->flush(); //exit;
        }

        $post->setUpdated(new \DateTime("now"));

        $post->uploadForListPhoto();
        $post->uploadForRightPhoto();
        $post->uploadForSocialPhoto();

    }

    private function removeSubstitude(&$substitudeOut, &$substitudeIn, $playerID, $em)
    {

        if(!empty($substitudeIn[$playerID])){

            // remove substitude In if not exist
            foreach($substitudeIn[$playerID] as $sIN){

                $em->remove($sIN);
            }
        }
        if(!empty($substitudeOut[$playerID])){

            // remove substitude Out if not exist
            foreach($substitudeOut[$playerID] as $sOut){

                $em->remove($sOut);
            }
        }
    }
}

