<?php
/**
 * Created by PhpStorm.
 * User: aram
 * Date: 7/5/15
 * Time: 1:11 PM
 */


namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class PlayerAdmin extends Admin
{

    protected  $baseRouteName = 'player';
    protected  $baseRoutePattern = 'player';


    /**
     * Row show configuration
     *
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     * @return void
     */
    protected function configureShowField(ShowMapper $showMapper)
    {
        $showMapper
            ->add('firstName', null, array('label' => 'First Name'));
    }

    /**
     * List show configuration
     *
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('firstName', null, array('label' => 'First Name'))
            ->addIdentifier('lastName', null, array('label' => 'Last Name'))
            ->add('playerTeams.team', null, array('label' => 'Current Team' , 'template' => 'AppBundle:Admin:list_team.html.twig'))
            ->add('playerTeams.number', null, array('label' => 'Number', 'template' => 'AppBundle:Admin:list_number.html.twig'))
                ->add('_action', 'actions', array('actions' => array(
                'show' => array(),
                'edit' => array(),
                'delete' => array()
            )));
    }

    /**
     * Row form edit configuration
     *
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('General')
            ->add('firstName', null, array('required' => true))
            ->add('lastName', null, array('required' => false))
            ->add('shortName', null, array('required' => false))
            ->add('playerTeams', 'sonata_type_collection', array ('by_reference' => false, 'required' => false), array (
                'edit' => 'inline',
                'expanded' => true,
                'admin_code' => 'fast.goals.main.admin.player.teams',
                'inline' => 'table',
            ))
            ->end();
    }

    /**
     * Fields in list rows search
     *
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('firstName')
            ->add('lastName')
            ->add('playerTeams.team.name', null, array('label'=>'Team'))
            ->add('slug');
    }


}