<?php
/**
 * Created by PhpStorm.
 * User: aram
 * Date: 7/5/15
 * Time: 1:15 PM
 */

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class TopPostAdmin extends Admin
{

    protected  $baseRouteName = 'top-post';
    protected  $baseRoutePattern = 'top-post';

    /**
     * Row show configuration
     *
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     * @return void
     */
    protected function configureShowField(ShowMapper $showMapper)
    {
        $showMapper
            ->add('title', null, array('label' => 'Title'))
            ->add('video', null, array('label' => 'Video url'))
            ->add('videoPlayWire', null, array('label' => 'video PlayWire'));
    }

    /**
     * List show configuration
     *
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('title', null, array('label' => 'Title'))
            ->add('video', null, array('label' => 'Video url'))
            ->add('slug', null, array('label' => 'Slug'))
            ->add('publish', 'boolean', array('label' => 'Publish'))
            ->add('position', null, array('required' => false))
            ->add('_action', 'actions', array('actions' => array(
                'show' => array(),
                'edit' => array(),
                'delete' => array()
            )));
    }

    /**
     * Row form edit configuration
     *
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('postDate', 'date', array('widget' => 'choice', 'required' => false,'years' => range(1980, 2030)))
            ->add('title', null, array('label' => 'Title'))
            ->add('slug', null, array('required' => false))
            ->add('video', null, array('label' => 'Video url',  'required' => false))
            ->add('videoDuration', null, array('label' => 'Video duration'))
            ->add('videoPlayWire', null, array('label' => ' PlayWire Url', 'required' => false))
            ->add('team', 'sonata_type_model_autocomplete',
                array(
                    'attr'=>array('class'=>'auto-class'),
                    'required' => false,
                    'property' => 'name',
                    'multiple'=> true,
                    'placeholder' => 'Select the team'
                ))
            ->add('player', 'sonata_type_model_autocomplete',
                array(
                    'required' => false,
                    'attr'=>array('class'=>'auto-class'),
                    'property' => array('firstName', 'lastName'),
                    'multiple'=> true,
                    'placeholder' => 'Select the team'
                ))
            ->add('season', 'sonata_type_model_autocomplete',
                array(
                    'attr'=>array('class'=>'auto-class'),
                    'required' => false,
                    'property' => 'name',
                    'multiple'=> true,
                    'placeholder' => 'Select the season'
                ))
            ->add('listPhotoFile', 'file', array('required' => false))
            ->add('rightPhotoFile', 'file', array('required' => false))
            ->add('socialPhotoFile', 'file', array('required' => false))

            ->add('meta_kw', null, array('label' => 'Meta keywords'))
            ->add('meta_desc', null, array('label' => 'Meta description'))
            ->add('position', null, array('required' => false))
            ->add('publish', null, array('required' => false))
            ->end();
    }

    /**
     * Fields in list rows search
     *
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
              ->add('title', null, array('label' => 'Title'))
              ->add('video', null, array('label' => 'Video url'));
    }

    /**
     * {@inheritdoc}
     */
    public function preUpdate($post)
    {
        $post->setUpdated(new \DateTime("now"));
        $post->uploadForListPhoto();
        $post->uploadForRightPhoto();
        $post->uploadForSocialPhoto();
    }

    /**
     * {@inheritdoc}
     */
    public function prePersist($post)
    {
        $post->uploadForListPhoto();
        $post->uploadForRightPhoto();
        $post->uploadForSocialPhoto();

    }

}