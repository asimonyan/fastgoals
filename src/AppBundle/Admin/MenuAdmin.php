<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class MenuAdmin extends Admin
{
    /**
     * Row show configuration
     *
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     * @return void
     */
    protected function configureShowField(ShowMapper $showMapper)
    {
        $showMapper
                ->add('name', null, array('label' => 'Name'))
                ->add('position', null, array('label' => 'Position'))
                ->add('slug', null, array('label' => 'Slug'));
    }

    /**
     * List show configuration
     *
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->addIdentifier('name', null, array('label' => 'Name'))
                ->addIdentifier('position', null, array('label' => 'Position'))
                ->addIdentifier('slug', null, array('label' => 'Slug'))
                ->add('_action', 'actions', array('actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array()
                )));
    }

    /**
     * Row form edit configuration
     *
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->with('General')
                ->add('name', null, array('required' => true))
                ->add('position', null, array('required' => false))
                ->add('slug', null, array('required' => false))
//                ->add('content', 'textarea', array('attr' => array('class' => 'tinymce', 'tinymce'=>'{"theme":"simple"}'), 'required' => false))
                ->add('content', 'textarea', array('required' => false))
                ->add('content', 'textarea', array('required' => false, 'attr' => array(
                'class' => 'tinymce',
                'data-theme' => 'bbcode' // Skip it if you want to use default theme
            )))
                ->end();
    }

    /**
     * Fields in list rows search
     *
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('name')
                ->add('slug')
                ->add('position');
    }

}