app.controller('maincontroller',function($scope,$resource,player,EditedPlayer){

    $scope.tmp = [];
    $scope.requestHome = 1;
    $scope.requestGuest = 1;

    $scope.$watch('teamHome',function(){
        if (angular.isDefined($scope.teamHome)){
            if ($scope.editStatus) {
                console.log("GET EDITED TEAMHome");
                EditedPlayer.getPlayer({teamId: $scope.teamHome,postId: $scope.postId},function(data){

                    delete data.wtf;

                    $scope.teamHomePlayers = [];
                    angular.forEach(data,function(val,key){
                        if (angular.isDefined(val.info)){
                            var tmp  = val.info;
                            angular.forEach(val.events,function(eventVal){
                                if (eventVal.type ==6)
                                    tmp['lineup'] = true;
                                if (eventVal.type == 1){
                                    if (angular.isDefined(tmp['goal'])){
                                        if (eventVal.own == 1){
                                            tmp['goal'] =tmp['goal'] + ','+ eventVal.minute + '-o';
                                        } else {
                                            tmp['goal'] =tmp['goal'] + ','+ eventVal.minute;
                                        }
                                    } else {
                                        if (eventVal.own == 1){
                                            tmp['goal'] = eventVal.minute + '-o';
                                        } else {
                                            tmp['goal'] = eventVal.minute;
                                        }
                                    }
                                }
                                if (eventVal.type == 2){
                                    if (angular.isDefined(tmp['yellow'])){
                                        tmp['yellow'] = tmp['yellow'] + ',' + eventVal.minute;
                                    } else {
                                        tmp['yellow'] = eventVal.minute;
                                    }
                                }
                                if (eventVal.type == 3)
                                    tmp['red'] = eventVal.minute;
                                if (eventVal.type == 4)
                                    tmp['subs'] = eventVal.minute;
                                if (eventVal.type == 5)
                                    tmp['subs'] = eventVal.minute;
                            });
                            $scope.teamHomePlayers.push(tmp);

                        }
                    });
                    $scope.teamHomeInfo = data;
                });
            } else {
                player.getPlayer({id: $scope.teamHome,stat: $scope.requestHome},function(data){

                    delete data.wtf;
                    delete data['$promise'];
                    delete data['$resolved'];

                    $scope.teamHomePlayers = [];

                    angular.forEach(data,function(val){
                        $scope.teamHomePlayers.push(val);
                    });

                    $scope.teamHomeInfo = data;
                });
            }
        }
    },true);

    $scope.$watch('teamGuest',function(){
        if (angular.isDefined($scope.teamGuest)){
            if ($scope.editStatus) {
                console.log("GET EDITED TEAMGuest");

                EditedPlayer.getPlayer({teamId: $scope.teamGuest,postId: $scope.postId},function(data){

                    delete data.wtf;

                    $scope.teamGuestPlayers = [];
                    angular.forEach(data,function(val,key){
                        if (angular.isDefined(val.info)){
                            var tmp  = val.info;
                            angular.forEach(val.events,function(eventVal){
                                if (eventVal.type ==6)
                                    tmp['lineup'] = true;
                                if (eventVal.type == 1){
                                    if (angular.isDefined(tmp['goal'])){
                                        if (eventVal.own == 1){
                                            tmp['goal'] =tmp['goal'] + ','+ eventVal.minute + '-o';
                                        } else {
                                            tmp['goal'] =tmp['goal'] + ','+ eventVal.minute;
                                        }
                                    } else {
                                        if (eventVal.own == 1){
                                            tmp['goal'] = eventVal.minute + '-o';
                                        } else {
                                            tmp['goal'] = eventVal.minute;
                                        }
                                    }
                                }
                                if (eventVal.type == 2){
                                    if (angular.isDefined(tmp['yellow'])){
                                        tmp['yellow'] = tmp['yellow'] + ',' + eventVal.minute;
                                    } else {
                                        tmp['yellow'] = eventVal.minute;
                                    }
                                }
                                if (eventVal.type == 3)
                                    tmp['red'] = eventVal.minute;
                                if (eventVal.type == 4)
                                    tmp['subs'] = eventVal.minute;
                                if (eventVal.type == 5)
                                    tmp['subs'] = eventVal.minute;
                            })
                            $scope.teamGuestPlayers.push(tmp);
                        }
                    });
                    $scope.teamGuestInfo = data;
                });
            } else {
                player.getPlayer({id: $scope.teamGuest,stat: $scope.requestGuest},function(data){

                    delete data.wtf;
                    delete data['$promise'];
                    delete data['$resolved'];

                    $scope.teamGuestPlayers = [];
                    angular.forEach(data,function(val){
                        $scope.teamGuestPlayers.push(val);
                    });
                    $scope.teamGuestInfo = data;
                });
            }
        }
    },true);


    $scope.initialize = function(postId,teamType,teamID){
        if (postId==0)
            $scope.editStatus = false;
        else {
            $scope.editStatus = true;
            $scope.postId = postId;
            if (teamType == 0)
                $scope.teamHome = teamID;
            else
                $scope.teamGuest = teamID;
        }
    }
});
