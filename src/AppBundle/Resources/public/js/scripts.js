var $window = $(window),
    $document = $(document),
    $body = $(document.body),
    $itemsList = $("#items-list .row"),
    $header = $("#header"),
    $leftSidebar = $("#left-sidebar"),
    noMoreItems = false;

$itemsList.masonry({
    columnWidth: 267,
//    columnWidth: ".item-sizer",
    gutter: 20,
    itemSelector: ".item",
    transitionDuration: "0.1s"
});

var templates = {
    "item": null,
    "watch-popup": null,
    "ad": null,
    "ad-google": null
};

var loadTemplate = function( templateName ) {
    var dfd = $.Deferred();
    $.get("/bundles/app/templates/" + templateName + ".html", function( source ) {
        templates[templateName] = _.template( source );
        dfd.resolve();
    });
    return dfd.promise();
};

var loadTemplates = function() {
    var dfdResults = [];

    _.forEach(templates, function( content, templateName ) {
        dfdResults.push(this.loadTemplate( templateName ));
    }, this);

    $.when.apply( null, dfdResults ).done( $.proxy(onLoadTemplates, this) );
};

var onLoadTemplates = function() {
    loadItems();
};

var itemsOffset = 0,
    itemsLimit = 12,
    loadedItems = [];

var firstCheck = true;

var loadItems = function() {
    var leagueId = $("#leagueId").val(),
        teamId = $("#teamId").val(),
        playerId = $("#playerId").val(),
        searchQuery = $("#searchQuery").val(),
        date = $("#date").val(),
        most  = $("#most").val();

    if ( noMoreItems ) {
        return false;
    }

    var data = {
        offset: itemsOffset,
        limit: itemsLimit
    };

    if ( searchQuery ) {
        _.extend(data, { q: searchQuery });
    }

    if ( leagueId ) {
        _.extend(data, { l: leagueId });
    }
    if ( teamId ) {
        _.extend(data, { t: teamId });
    }
    if ( playerId ) {
        _.extend(data, { p: playerId });
    }
    if ( date != "" && date != "all") {
        _.extend(data, { d: date });
    }
    if ( most && most != 'most' ) {
        _.extend(data, { m: most });
    }

    $.getJSON(hostName + "/api/items", data, function(items) {

        if (firstCheck) {
            firstCheck = false;
            if (items.length == 0){
                $(".search-error").show();
            }
        }

        var $itemNode,
            itemNodes = [];

        var testId = 0;

        function test(item){
            item['baseUrl'] = hostName;
            if ( !item.isAd ) {
                $itemNode = $(templates["item"]( item ));
            }
            else {
                if (!item.isAdGoogle){
                    $itemNode = $(templates["ad"]( item ));
                } else{
                    $itemNode = $(templates["ad-google"]( item ));
                }
            }
            itemNodes.push( $itemNode.get(0) );
        }

        function loadTest(){
            window.setTimeout(function(){
                test(items[testId]);
                testId++;
                if (testId<items.length) loadTest();
                else {
                    $itemsList.append( itemNodes);

                    $itemsList.masonry("appended", itemNodes);

                    loadedItems = _.union(loadedItems, items);

                    itemsOffset += itemsLimit;
                    if ( !items.length ) {
                        hasItemsToLoad = false;
                    }
                                    }
            },0);
        }

        if ( items.length ) {
            loadTest();
        }
        else {
            noMoreItems = true;
        }
    });
};
loadTemplates();

var getLoadedItem = function( itemId ) {
    var loadedItem = null;
    _.forEach(loadedItems, function( item ) {
        if ( item.id == itemId ) {
            loadedItem = item;
            return false;
        }
    });
    return loadedItem;
};

var getNextLoadedItem = function ( itemId ) {
    var index = -1;
    _.forEach(loadedItems, function( item, i ) {
        if ( item.id == itemId ) {
            index = i;
            return false;
        }
    });
    return loadedItems[index+1] || null;
};
var getPrevLoadedItem = function ( itemId ) {
    var index = -1;
    _.forEach(loadedItems, function( item, i ) {
        if ( item.id == itemId ) {
            index = i;
            return false;
        }
    });
    return loadedItems[index-1] || null;
};


var disablePageScrolling = function() {
    $window.on("mousewheel.ddMenu", function() { return false; });
};
var enablePageScrolling = function() {
    $window.off("mousewheel.ddMenu");
};



var openNextWatchPopup = function ( item ) {
    closeWatchPopup();
    openWatchPopup( item );
};
var openWatchPopup = function( item ) {
    var prevItem = getPrevLoadedItem( item.id ),
        nextItem = getNextLoadedItem( item.id );

    $.get(hostName + "/api/items/views/update", { postSlug: item.slug });

    _.extend(item, {
        pageURL: encodeURIComponent(item.videoUrl),
        views: ++item.views
    });

    $(templates["watch-popup"]( item )).appendTo( $body );

    if (item.videoUrl == ""){
        $("#youtubeVideo")[0].style.display = "none";
    } else {
        $("#youtubeVideo")[0].style.display = "block";
    }

    twttr.widgets.load();
    gapi.plusone.render("g-plusone", {
        "data-href": "http://vimeo.com",
        "data-size": "small"
    });

    $("#watch-popup-close").one("click", closeWatchPopup);
    $("#watch-popup-prev").one("click", function() {
        openNextWatchPopup( prevItem );
    }).toggleClass("hide", !prevItem);
    $("#watch-popup-next").one("click", function() {
        openNextWatchPopup( nextItem );
    }).toggleClass("hide", !nextItem);
    disablePageScrolling();
};
var closeWatchPopup = function() {
    $("#watch-popup-container").remove();
    enablePageScrolling();
    return false;
};

var hasItemsToLoad = true;
$window.on("scroll", function() {
    var loadHeight = $document.height() - $window.height(),
        scrollTop = $window.scrollTop();

    if ( hasItemsToLoad && scrollTop >= loadHeight ) {
        loadItems();
    }

    $header.toggleClass("collapsed", scrollTop > 0);

    $leftSidebar.css({
        top: scrollTop + 70
    });
});

// Expect input as d/m/y
var isValidDate = function(s) {
    if ( s != undefined){
        var bits = s.split('.');
        var d = new Date(bits[2], bits[1] - 1, bits[0]);
        return d && (d.getMonth() + 1) == bits[1] && d.getDate() == Number(bits[0]);
    } else {
        return true;
    }
};

$("#calendar-date").datepicker({
    onSelect: function(dateText) {
        var selectedDate = new Date(dateText),
            day = selectedDate.getDate(),
            month = selectedDate.getMonth() + 1,
            year = selectedDate.getFullYear(),
            url = $("#currentPath").val();

        if (day < 10) { day = "0" + day; }
        if (month < 10) { month = "0" + month; }

        url = url.split("/").clean('');

        var urlDateValid = isValidDate(url[0]);

        url[0] = [day, month, year].join(".");

        //if(url.length == 0 || urlDateValid){
        //    url[0] = [day, month, year].join(".");
        //}
        //else{
        //    url[1] = [day, month, year].join(".");
        //}

        url = url.join("/");

        document.location.href = '/' + url;
    }
});

var $activeDropDown = null,
    $activeItem = null,
    $overlayLayer = null;


$(".dd-menu").on("click", "a.item", function() {
    var $item = $(this);

    if ( $item.hasClass("opened") ) {
        closeSubMenu();
        return false;
    }

    if ( !$item.hasClass("selected") ) {
        if ( $activeDropDown ) {
            closeSubMenu();
        }

        $activeDropDown = $item.parent().find("div.dropdown").removeClass("hide");
        $activeItem = $item.addClass("opened");

        if ( $activeDropDown.hasClass("left") ) {
            $activeDropDown.find(".arrow").css("left", $item.position().left + $item.width() + 3);
        }
        else if ( $activeDropDown.hasClass("right") ) {
            $activeDropDown.find(".arrow").css("right", 838 - $item.position().left - $item.width() + 3);
        }
        else {
            $activeDropDown.css("left", $item.position().left);
            $activeDropDown.find(".arrow").css("left", $item.width() + 3);
        }

        $overlayLayer = $('<div class="overlayLayer"></div>').appendTo( $body );
        disablePageScrolling();

        $(document).on("click", function( e ) {
            if ( !$(e.target).closest("div").hasClass("ui-datepicker-header") ) {
                if ( (!$(e.target).closest("div.dropdown").length) ) {
                    closeSubMenu();
                }
            }
        });

        $activeDropDown.find(":input:first").focus();

        return false;
    }
});

function closeSubMenu() {
    if ( $activeDropDown ) {
        $activeItem.removeClass("opened");
        $activeDropDown.addClass("hide");
        $activeDropDown = null;
        $(document).off("click");
        enablePageScrolling();
        $overlayLayer.remove();
    }
}

$itemsList.on("click", ".watch-button", function() {
    var itemId = $(this).data("id"),
        item = getLoadedItem( itemId );

    if ( item ) {
        openWatchPopup( item );
    }

    return false;
});

$itemsList.on("click", ".watch-img", function() {
    var itemId, item;

    if ( $(this).find(".watch-box").is(":visible") ) {
        itemId = $(this).data("id");
        item = getLoadedItem(itemId);

        if ( item ) {
            openWatchPopup( item );
        }
    }
    else {
        location.href = $(this).closest(".item").find(".item-more-button").attr("href");
    }

    return false;
});

$("#search-icon").on("click", function() {
    $("#search-block").toggleClass("expanded");
});

$(function() {
    var num_cols = 4,
        container = $(".split-list"),
        listClass = "sub-list";

    container.each(function() {
        var items_per_col = [],
            items = $(this).find("li"),
            min_items_per_col = Math.floor(items.length / num_cols),
            difference = items.length - (min_items_per_col * num_cols),
            i = 0;

        for (i = 0; i < num_cols; i++) {
            if (i < difference) {
                items_per_col[i] = min_items_per_col + 1;
            } else {
                items_per_col[i] = min_items_per_col;
            }
        }
        for (i = 0; i < num_cols; i++) {
            $(this).append($('<ul></ul>').addClass(listClass));
            for (var j = 0; j < items_per_col[i]; j++) {
                var pointer = 0;
                for (var k = 0; k < i; k++) {
                    pointer += items_per_col[k];
                }
                $(this).find("." + listClass).last().append(items[j + pointer]);
            }
        }
    });
});

$(function() {
    var playerCache = {},
        teamCache = {};

    $("input.player-search-input").autocomplete({
        position: { my: "left top", at: "left bottom-3", collision: "none" },
        source: function( request, response ) {
            var leagueId = $("#leagueId").val(),
                teamId = $("#teamId").val(),
                date = $("#date").val(),
                most = $("#most").val(),
                term = request.term;

            var data = {
                term: term
            };

            if ( leagueId ) {
                _.extend(data, { leagueId: leagueId });
            }
            if ( teamId ) {
                _.extend(data, { teamId: teamId });
            }
            if ( date ) {
                _.extend(data, { date: date });
            }

            if ( most ) {
                _.extend(data, { most: most });
            }


            if ( term in playerCache ) {
                response( playerCache[term] );
                return;
            }

            $.getJSON(hostName + "/api/players/search", data, function( data, status ) {
                playerCache[term] = data;
                response( data );
            });
        },
        focus: function() {
            return false;
        },
        select: function( event, ui ) {
            var leagueSlug = $("#categorySlug").val(),
                teamSlug = $("#teamSlug").val(),
                dateSlug = $("#date").val(),
                most = $("#most").val();

            document.location.href = hostName + "/"+dateSlug+"/"+leagueSlug+"/"+teamSlug+"/"+ui.item.id+"/"+most;
        }
    });

    $("input.team-search-input").autocomplete({
        position: { my: "left top", at: "left bottom-3", collision: "none" },
        source: function( request, response ) {
            var leagueId = $("#leagueId").val(),
                date = $("#date").val(),
                most = $("#most").val(),
                playerId = $("#playerId").val(),
                term = request.term;

            var data = {
                term: term
            };

            if ( leagueId ) {
                _.extend(data, { leagueId: leagueId });
            }

            if ( playerId ) {
                _.extend(data, { playerId: playerId });
            }

            if ( date ) {
                _.extend(data, { date: date });
            }

            if ( most ) {
                _.extend(data, { most: most });
            }

            if ( term in teamCache ) {
                response( teamCache[term] );
                return;
            }

            $.getJSON(hostName + "/api/teams/search", data, function( data, status ) {
                teamCache[term] = data;
                response( data );
            });
        },
        focus: function() {
            return false;
        },
        select: function( event, ui ) {
            var leagueSlug = $("#categorySlug").val(),
                dateSlug = $("#date").val(),
                playerSlug = $("#playerSlug").val(),
                most = $("#most").val();

            document.location.href = hostName + "/"+dateSlug+"/"+leagueSlug+"/"+ui.item.id + "/" + playerSlug + "/" +most;
        }
    });
});


// this function is used to remove empty element from array

Array.prototype.clean = function(deleteValue) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == deleteValue) {
            this.splice(i, 1);
            i--;
        }
    }
    return this;
};