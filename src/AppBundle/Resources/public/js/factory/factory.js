app.factory("player", function($resource) {
    return $resource('/api/teams/:id/statuses/:stat', {}, {
        getPlayer: {
            method: "GET"
        }
    });
});

app.factory("EditedPlayer", function($resource) {
    return $resource('/api/items/:teamId/teams/:postId/post', {}, {
        getPlayer: {
            method: "GET",isArray:true
        }
    });
});