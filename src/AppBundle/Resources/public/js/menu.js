'use strict';
angular.module('menu.accordion', ['ui.bootstrap','Google','Twitter','Facebook']);
angular.module('menu.accordion').controller('menuAccordionCtrl', function ($scope,$window,$filter) {

    $scope.oneAtATime = true;

    $scope.firstTime = true;

    $scope.setValue = function(val){
        if (val != 'date')
            $scope.dt.value = new Date( val.replace( /(\d{2}).(\d{2}).(\d{4})/, "$2/$1/$3") );
    };

    $scope.today = function() {

        $("#mobile-menu")[0].style.visibility = "visible";

        $scope.dt = {};
        $scope.$watch('dt.value', function(value) {
            console.log(value);
            if (angular.isDefined(value)){
                if ($scope.firstTime)
                    $scope.firstTime = false;
                else {
                    $scope.date = $filter('date')(value, 'dd.MM.yyyy');
                    $scope.url = $("#currentPath").val();

                    $scope.url = $scope.url.split("/").clean('');

                    if($scope.url.length == 0 || $scope.url[0] == 'date'){
                        $scope.url[0] = $scope.date;
                    }
                    else{
                        $scope.url[1] = $scope.date;
                    }


//                    $scope.url[1] = $scope.date;
                    $scope.url = $scope.url.join("/");
                    document.location.href = '/' + $scope.url;
                }
            } else {
                $scope.firstTime = false;
            }
        });
    };
    $scope.today();

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];




});