/**
 * Created by aram on 7/21/15.
 */

$(function (data)
{

    // Radialize the colors
    Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
        return {
            radialGradient: {
                cx: 0.5,
                cy: 0.3,
                r: 0.7
            },
            stops: [
                [0, color],
                [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
            ]
        };
    });

    var firstChart = $($('.chart-data')[0]).children('li');
    var isColored = 0;


    // Build the chart
    $('#highchart').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            width: $(window).width() <= 320 ? 250:300,
            height: $(window).width() <= 320 ? 250:300
        },
        exporting: { enabled: false },
        credits: { enabled: false },
        title: {
            text: ''
        },
        //tooltip: {
        //    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        //},
        plotOptions: {
            pie: {
                allowPointSelect: true,
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
                //cursor: 'pointer',
                //dataLabels: {
                //    enabled: true,
                //    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                //    style: {
                //        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                //    },
                //    connectorColor: 'silver'
                //}
            }
        },
        colors: Highcharts.map(Highcharts.getOptions().colors, function(color) {
            if(isColored == 1){
                color.stops[0][1] = '#B2CA86';
                color.stops[1][1] = '#84AD33';
            }
            isColored++;
            return color;
        }),
        series: [{
            name: "Value",
            data: [
                {
                    name: $(".chart-total .chart-title li:first-child").text(),
                    y: Number($(firstChart[0]).text())
                },
                {
                    name: $(".chart-total .chart-title li:nth-child(2)").text(),
                    y: Number($(firstChart[2]).text()),
                    sliced: true,
                    selected: true
                }
            ]
        }]
    });

    $('.chart-data').click(function(){
        $('.chart-data').removeClass("active-stat");
        $(this).addClass("active-stat");
        //var chart = $('#highchart').highcharts();
        var chart = $('#highchart').highcharts();
        var num1 = Number($($(this).children('li')[0]).text());
        var num2 = Number($($(this).children('li')[2]).text());
        chart.series[0].setData([
            {y: num1},
            {y: num2,sliced: true,selected: true}]
        );
    });

});