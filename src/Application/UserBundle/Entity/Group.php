<?php
/**
 * Created by PhpStorm.
 * User: aram
 * Date: 6/6/15
 * Time: 4:23 PM
 */

namespace Application\UserBundle\Entity;

use Sonata\UserBundle\Entity\BaseGroup as BaseGroup;
use Doctrine\ORM\Mapping as ORM;
/**
 *
 * @ORM\Table(name="fos_user_groups")
 * @ORM\Entity()
 *
 */
class Group extends BaseGroup
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}