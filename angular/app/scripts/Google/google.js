'use strict';

angular.module('Google',[])
    .directive("googleShareAlt",[function(){

        var href = "https://plus.google.com/share?url=__url__";
        var onclick = "javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;";

        return {
            restrict: 'EA',
            scope: {},
            compile: function(el,attrs){
                href = href.replace('__url__',attrs.url);
                attrs.$set('href',href);
                attrs.$set('onclick',onclick);
            }
        }
    }]);