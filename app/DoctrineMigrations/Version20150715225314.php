<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150715225314 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

//        $this->addSql('ALTER TABLE posts DROP FOREIGN KEY FK_885DBAFAA21214B7');
//        $this->addSql('ALTER TABLE teams_cat DROP FOREIGN KEY FK_6E56C2CA21214B7');
//        $this->addSql('ALTER TABLE teams_dates DROP FOREIGN KEY FK_5F4A31B3A21214B7');
//        $this->addSql('ALTER TABLE posts DROP FOREIGN KEY FK_885DBAFAEB573BFA');
//        $this->addSql('ALTER TABLE teams DROP FOREIGN KEY FK_96C22258F92F3E70');
//        $this->addSql('ALTER TABLE top_posts DROP FOREIGN KEY FK_1D42138EF92F3E70');
//        $this->addSql('ALTER TABLE events DROP FOREIGN KEY FK_5387574A99E6F5DF');
//        $this->addSql('ALTER TABLE player_teams DROP FOREIGN KEY FK_29B0591E99E6F5DF');
//        $this->addSql('ALTER TABLE top_posts DROP FOREIGN KEY FK_1D42138E99E6F5DF');
//        $this->addSql('ALTER TABLE events DROP FOREIGN KEY FK_5387574A4B89032C');
//        $this->addSql('ALTER TABLE events DROP FOREIGN KEY FK_5387574A296CD8AE');
//        $this->addSql('ALTER TABLE player_teams DROP FOREIGN KEY FK_29B0591E296CD8AE');
//        $this->addSql('ALTER TABLE posts DROP FOREIGN KEY FK_885DBAFA6BB1D375');
//        $this->addSql('ALTER TABLE posts DROP FOREIGN KEY FK_885DBAFAA037F8AB');
//        $this->addSql('ALTER TABLE team_dates DROP FOREIGN KEY FK_6C72805ED6365F12');
//        $this->addSql('ALTER TABLE teams_cat DROP FOREIGN KEY FK_6E56C2CD6365F12');
//        $this->addSql('ALTER TABLE top_posts DROP FOREIGN KEY FK_1D42138E296CD8AE');
//        $this->addSql('ALTER TABLE team_dates DROP FOREIGN KEY FK_6C72805EE12E3F0A');
//        $this->addSql('DROP TABLE ads');
//        $this->addSql('DROP TABLE categories');
//        $this->addSql('DROP TABLE categories_dates');
//        $this->addSql('DROP TABLE country');
//        $this->addSql('DROP TABLE events');
//        $this->addSql('DROP TABLE media_translations');
//        $this->addSql('DROP TABLE player_teams');
//        $this->addSql('DROP TABLE players');
//        $this->addSql('DROP TABLE posts');
//        $this->addSql('DROP TABLE team_dates');
//        $this->addSql('DROP TABLE teams');
//        $this->addSql('DROP TABLE teams_cat');
//        $this->addSql('DROP TABLE teams_dates');
//        $this->addSql('DROP TABLE top_posts');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE ads (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, slug VARCHAR(50) NOT NULL, position VARCHAR(3) NOT NULL, url VARCHAR(255) NOT NULL, width VARCHAR(3) NOT NULL, height VARCHAR(3) NOT NULL, path VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_7EC9F620989D9B62 (slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE categories (id INT AUTO_INCREMENT NOT NULL, image_id INT DEFAULT NULL, categories_name VARCHAR(200) DEFAULT NULL, categories_desc VARCHAR(250) DEFAULT NULL, slug VARCHAR(100) NOT NULL, position VARCHAR(10) DEFAULT NULL, UNIQUE INDEX UNIQ_3AF34668989D9B62 (slug), INDEX IDX_3AF346683DA5256D (image_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE categories_dates (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(200) DEFAULT NULL, slug VARCHAR(100) NOT NULL, start_date DATE NOT NULL, end_date DATE NOT NULL, UNIQUE INDEX UNIQ_AF77DC53989D9B62 (slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE country (id INT AUTO_INCREMENT NOT NULL, country_name VARCHAR(150) DEFAULT NULL, slug VARCHAR(100) NOT NULL, UNIQUE INDEX UNIQ_5373C966989D9B62 (slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE events (id INT AUTO_INCREMENT NOT NULL, team_id INT DEFAULT NULL, post_id INT DEFAULT NULL, player_id INT DEFAULT NULL, type INT NOT NULL, minute INT NOT NULL, own INT DEFAULT 0 NOT NULL, INDEX IDX_5387574A4B89032C (post_id), INDEX IDX_5387574A296CD8AE (team_id), INDEX IDX_5387574A99E6F5DF (player_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE media_translations (id INT AUTO_INCREMENT NOT NULL, object_id INT DEFAULT NULL, locale VARCHAR(8) NOT NULL, field VARCHAR(32) NOT NULL, content LONGTEXT DEFAULT NULL, UNIQUE INDEX media_trans_lookup_unique_idx (locale, object_id, field), INDEX IDX_AF46700B232D562B (object_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE player_teams (id INT AUTO_INCREMENT NOT NULL, team_id INT DEFAULT NULL, player_id INT DEFAULT NULL, status INT NOT NULL, start_date DATE NOT NULL, end_date DATE NOT NULL, number INT DEFAULT NULL, INDEX IDX_29B0591E99E6F5DF (player_id), INDEX IDX_29B0591E296CD8AE (team_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE players (id INT AUTO_INCREMENT NOT NULL, first_name VARCHAR(100) DEFAULT NULL, last_name VARCHAR(150) DEFAULT NULL, short_name VARCHAR(50) DEFAULT NULL, number INT DEFAULT NULL, slug VARCHAR(100) NOT NULL, tags INT NOT NULL, UNIQUE INDEX UNIQ_264E43A6989D9B62 (slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE posts (id INT AUTO_INCREMENT NOT NULL, team_guest INT DEFAULT NULL, team_home INT DEFAULT NULL, categories_id INT DEFAULT NULL, categories_dates_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, video_url VARCHAR(255) NOT NULL, created DATETIME NOT NULL, updated DATETIME NOT NULL, slug VARCHAR(50) NOT NULL, score VARCHAR(10) NOT NULL, video_duration VARCHAR(20) NOT NULL, posts_date DATE NOT NULL, path VARCHAR(255) DEFAULT NULL, path1 VARCHAR(255) DEFAULT NULL, meta_desc VARCHAR(300) DEFAULT NULL, meta_kw VARCHAR(200) DEFAULT NULL, views INT NOT NULL, UNIQUE INDEX UNIQ_885DBAFA989D9B62 (slug), INDEX IDX_885DBAFAA037F8AB (team_home), INDEX IDX_885DBAFA6BB1D375 (team_guest), INDEX IDX_885DBAFAA21214B7 (categories_id), INDEX IDX_885DBAFAEB573BFA (categories_dates_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE team_dates (teams_id INT NOT NULL, teamsdates_id INT NOT NULL, INDEX IDX_6C72805ED6365F12 (teams_id), INDEX IDX_6C72805EE12E3F0A (teamsdates_id), PRIMARY KEY(teams_id, teamsdates_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE teams (id INT AUTO_INCREMENT NOT NULL, country_id INT DEFAULT NULL, team_name VARCHAR(150) DEFAULT NULL, slug VARCHAR(100) NOT NULL, tags INT NOT NULL, UNIQUE INDEX UNIQ_96C22258989D9B62 (slug), INDEX IDX_96C22258F92F3E70 (country_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE teams_cat (teams_id INT NOT NULL, categories_id INT NOT NULL, INDEX IDX_6E56C2CD6365F12 (teams_id), INDEX IDX_6E56C2CA21214B7 (categories_id), PRIMARY KEY(teams_id, categories_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE teams_dates (id INT AUTO_INCREMENT NOT NULL, categories_id INT DEFAULT NULL, name VARCHAR(200) DEFAULT NULL, slug VARCHAR(100) NOT NULL, start_date DATE NOT NULL, end_date DATE NOT NULL, UNIQUE INDEX UNIQ_5F4A31B3989D9B62 (slug), INDEX IDX_5F4A31B3A21214B7 (categories_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE top_posts (id INT AUTO_INCREMENT NOT NULL, team_id INT DEFAULT NULL, player_id INT DEFAULT NULL, country_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, slug VARCHAR(50) NOT NULL, video_url VARCHAR(255) NOT NULL, start_date DATE NOT NULL, created DATETIME NOT NULL, updated DATETIME NOT NULL, UNIQUE INDEX UNIQ_1D42138E989D9B62 (slug), INDEX IDX_1D42138EF92F3E70 (country_id), INDEX IDX_1D42138E296CD8AE (team_id), INDEX IDX_1D42138E99E6F5DF (player_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE categories ADD CONSTRAINT FK_3AF346683DA5256D FOREIGN KEY (image_id) REFERENCES media__media (id)');
        $this->addSql('ALTER TABLE events ADD CONSTRAINT FK_5387574A296CD8AE FOREIGN KEY (team_id) REFERENCES teams (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE events ADD CONSTRAINT FK_5387574A4B89032C FOREIGN KEY (post_id) REFERENCES posts (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE events ADD CONSTRAINT FK_5387574A99E6F5DF FOREIGN KEY (player_id) REFERENCES players (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE media_translations ADD CONSTRAINT FK_AF46700B232D562B FOREIGN KEY (object_id) REFERENCES media__media (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE player_teams ADD CONSTRAINT FK_29B0591E296CD8AE FOREIGN KEY (team_id) REFERENCES teams (id)');
        $this->addSql('ALTER TABLE player_teams ADD CONSTRAINT FK_29B0591E99E6F5DF FOREIGN KEY (player_id) REFERENCES players (id)');
        $this->addSql('ALTER TABLE posts ADD CONSTRAINT FK_885DBAFA6BB1D375 FOREIGN KEY (team_guest) REFERENCES teams (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE posts ADD CONSTRAINT FK_885DBAFAA037F8AB FOREIGN KEY (team_home) REFERENCES teams (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE posts ADD CONSTRAINT FK_885DBAFAA21214B7 FOREIGN KEY (categories_id) REFERENCES categories (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE posts ADD CONSTRAINT FK_885DBAFAEB573BFA FOREIGN KEY (categories_dates_id) REFERENCES categories_dates (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE team_dates ADD CONSTRAINT FK_6C72805ED6365F12 FOREIGN KEY (teams_id) REFERENCES teams (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE team_dates ADD CONSTRAINT FK_6C72805EE12E3F0A FOREIGN KEY (teamsdates_id) REFERENCES teams_dates (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE teams ADD CONSTRAINT FK_96C22258F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE teams_cat ADD CONSTRAINT FK_6E56C2CA21214B7 FOREIGN KEY (categories_id) REFERENCES categories (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE teams_cat ADD CONSTRAINT FK_6E56C2CD6365F12 FOREIGN KEY (teams_id) REFERENCES teams (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE teams_dates ADD CONSTRAINT FK_5F4A31B3A21214B7 FOREIGN KEY (categories_id) REFERENCES categories (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE top_posts ADD CONSTRAINT FK_1D42138E296CD8AE FOREIGN KEY (team_id) REFERENCES teams (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE top_posts ADD CONSTRAINT FK_1D42138E99E6F5DF FOREIGN KEY (player_id) REFERENCES players (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE top_posts ADD CONSTRAINT FK_1D42138EF92F3E70 FOREIGN KEY (country_id) REFERENCES country (id) ON DELETE SET NULL');
    }
}
