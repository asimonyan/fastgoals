<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150715214320 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs

//                $this->addSql('INSERT INTO category(id, name, description, position, slug)
//                       SELECT id, categories_name, categories_desc, position, slug from categories');
//
//        $this->addSql('INSERT INTO season(id, name, slug, category_id, start_date, end_date)
//                       SELECT id, name, slug, categories_id, start_date, end_date from teams_dates');
//
//        $this->addSql('INSERT INTO team(id, name, slug)
//                       SELECT id, team_name, slug from teams');
//
//        $this->addSql('INSERT INTO player(id, first_name, last_name, short_name, slug)
//                       SELECT id, first_name, last_name, short_name, slug from players');
//
//        $this->addSql('INSERT INTO teams_season(team_id, season_id)
//                       SELECT teams_id, (SELECT id from season where season.category_id = teams_cat.categories_id ) from teams_cat
//                       where teams_cat.categories_id in ( SELECT category_id from season ) ');
//
//        $this->addSql('INSERT INTO player_team(id, player_id, team_id, status, start_date, end_date, number)
//                       SELECT id, player_id, team_id, status, start_date, end_date, number from player_teams');
//
//        $this->addSql('update player_team set number = (SELECT number from players WHERE players.id = player_team.player_id )');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
